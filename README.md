# php-extended/php-api-fr-insee-cog-builder

A library that provides the raw data to the php-api-fr-insee-cog-object library.
From [https://www.insee.fr/fr/information/2560452](https://www.insee.fr/fr/information/2560452).

![coverage](https://gitlab.com/php-extended/php-api-fr-insee-cog-builder/badges/master/pipeline.svg?style=flat-square)
![build status](https://gitlab.com/php-extended/php-api-fr-insee-cog-builder/badges/master/coverage.svg?style=flat-square)


## Installation

The installation of this library is made via composer and the autoloading of
all classes of this library is made through their autoloader.

- Download `composer.phar` from [their website](https://getcomposer.org/download/).
- Then run the following command to install this library as dependency :
- `php composer.phar php-extended/php-api-fr-insee-cog-builder ^8`


## Basic Usage

This library is made to be used in tandem with `php-extended/php-api-fr-insee-cog-object` for it to be regenerated.

Both of the libraries should be in the same directory, then just launch the
`./download.php` script with the following commands :


```bash

php download.php pays
php download.php paysHistory
php download.php regions
php download.php departements
php download.php arrondissements
php download.php cantons
php download.php communes
php download.php eventCommunes

```

This will regenerate all the files for all known years.


## License

MIT (See [license file](LICENSE)).
