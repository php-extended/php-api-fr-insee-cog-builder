<?php

// year min => year max => id canton => id cheflieu
// used in \PhpExtended\Insee\InseeCorrectionForeignKey
return [
	2016 => [
		2019 => [
			'4803' => '48099',
		],
		2020 => [
			'5012' => '50129',
			'5014' => '50129',
			'5024' => '50110',
		],
	],
	2017 => [
		2020 => [
			'7403' => '74010',
			'7416' => '74010',
		],
	],
];
