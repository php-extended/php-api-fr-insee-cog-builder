<?php

// @todo deprecated
return [
	2015 => [
		'0401' => '041',
		'1315' => '133',
		'1320' => '133',
		'2A03' => '2A1',
		'2A04' => '2A1',
		'2A05' => '2A1',
		'2A08' => '2A4',
		'2A09' => '2A4',
		'2A10' => '2A4',
		'2A11' => '2A4',
		'2B01' => '2B2',
		'2B02' => '2B2',
		'2B06' => '2B5',
		'2B07' => '2B5',
		'2B13' => '2B3',
		'2B15' => '2B3',
		'2808' => '282',
		'3817' => '382',
		'6912' => '691',
		'7628' => '762',
		'9112' => '913',
	],
	2019 => [
		'0189' => '011',
		'0199' => '012',
		'0297' => '021',
		'0298' => '022',
		'0299' => '025',
		'0397' => '031',
		'0398' => '031',
		'0399' => '031',
		'0498' => '041',
		'0499' => '042',
		'0598' => '051',
		'0599' => '052',
		'0694' => '061',
		'0695' => '061',
		'0696' => '062',
		'0697' => '062',
		'0698' => '062',
		'0699' => '062',
		'0798' => '073',
		'0799' => '071',
		'0888' => '082',
		'0889' => '083',
		'0898' => '083',
		'0899' => '084',
		'0999' => '091',
		'1099' => '103',
		'1198' => '111',
		'1199' => '112',
		'1298' => '122',
		'1299' => '122',
		'1397' => '131',
		'1398' => '133',
		'1399' => '132',
		'1489' => '144',
		'1499' => '143',
		'1588' => '153',
		'1589' => '152',
		'1598' => '151',
		'1599' => '151',
		'1689' => '161',
		'1698' => '161',
		'1699' => '163',
		'1789' => '172',
		'1799' => '172',
		'1898' => '183',
		'1899' => '182',
		'1989' => '191',
		'1999' => '191',
		'2199' => '212',
		'2289' => '221',
		'2299' => '221',
		'2399' => '231',
		'2487' => '241',
		'2488' => '243',
		'2489' => '241',
		'2498' => '241',
		'2499' => '241',
		'2587' => '253',
		'2588' => '252',
		'2589' => '253',
		'2599' => '251',
		'2697' => '261',
		'2698' => '263',
		'2699' => '262',
		'2783' => '271',
		'2784' => '272',
		'2785' => '272',
		'2786' => '271',
		'2787' => '271',
		'2788' => '273',
		'2789' => '273',
		'2799' => '272',
		'2888' => '282',
		'2889' => '282',
		'2898' => '282',
		'2899' => '282',
		'2998' => '294',
		'2999' => '291',
		'2A98' => '2A1',
		'2A99' => '2A1',
		'2B99' => '2B2',
		'3098' => '301',
		'3099' => '301',
		'3199' => '313',
		'3299' => '322',
		'3389' => '332',
		'3396' => '336',
		'3397' => '336',
		'3398' => '333',
		'3399' => '332',
		'3498' => '341',
		'3499' => '342',
		'3589' => '352',
		'3597' => '354',
		'3598' => '353',
		'3599' => '352',
		'3689' => '364',
		'3699' => '362',
		'3799' => '373',
		'3897' => '381',
		'3898' => '381',
		'3899' => '382',
		'3981' => '392',
		'3982' => '392',
		'3983' => '392',
		'3984' => '392',
		'3985' => '391',
		'3986' => '393',
		'3987' => '392',
		'3988' => '392',
		'3989' => '392',
		'3998' => '393',
		'3999' => '392',
		'4098' => '402',
		'4099' => '402',
		'4189' => '413',
		'4199' => '411',
		'4298' => '421',
		'4299' => '422',
		'4399' => '432',
		'4489' => '442',
		'4496' => '441',
		'4497' => '442',
		'4498' => '442',
		'4499' => '442',
		'4599' => '452',
		'4687' => '462',
		'4688' => '461',
		'4689' => '463',
		'4698' => '462',
		'4699' => '461',
		'4797' => '471',
		'4798' => '473',
		'4799' => '472',
		'4887' => '482',
		'4888' => '481',
		'4889' => '482',
		'4899' => '482',
		'4984' => '494',
		'4985' => '493',
		'4986' => '494',
		'4987' => '492',
		'4988' => '493',
		'4989' => '494',
		'4998' => '491',
		'4999' => '492',
		'5080' => '502',
		'5081' => '501',
		'5082' => '501',
		'5083' => '502',
		'5084' => '501',
		'5085' => '503',
		'5086' => '501',
		'5087' => '504',
		'5088' => '504',
		'5089' => '504',
		'5098' => '504',
		'5099' => '502',
		'5197' => '512',
		'5198' => '513',
		'5199' => '512',
		'5298' => '521',
		'5299' => '523',
		'5388' => '533',
		'5389' => '533',
		'5399' => '533',
		'5498' => '542',
		'5499' => '544',
		'5598' => '551',
		'5599' => '551',
		'5689' => '561',
		'5698' => '563',
		'5699' => '563',
		'5798' => '578',
		'5799' => '576',
		'5899' => '581',
		'5996' => '594',
		'5997' => '593',
		'5998' => '591',
		'5999' => '595',
		'6098' => '603',
		'6099' => '602',
		'6187' => '612',
		'6188' => '612',
		'6189' => '612',
		'6197' => '611',
		'6198' => '612',
		'6199' => '612',
		'6296' => '625',
		'6297' => '623',
		'6298' => '624',
		'6299' => '621',
		'6389' => '635',
		'6399' => '634',
		'6496' => '642',
		'6497' => '642',
		'6498' => '641',
		'6499' => '643',
		'6598' => '652',
		'6599' => '653',
		'6699' => '663',
		'6789' => '674',
		'6799' => '674',
		'6898' => '682',
		'6899' => '681',
		'6988' => '691',
		'6989' => '691',
		'7088' => '702',
		'7097' => '702',
		'7098' => '702',
		'7099' => '702',
		'7196' => '711',
		'7197' => '712',
		'7198' => '715',
		'7199' => '713',
		'7299' => '722',
		'7397' => '733',
		'7398' => '732',
		'7399' => '731',
		'7489' => '742',
		'7499' => '744',
		'7689' => '761',
		'7695' => '763',
		'7696' => '761',
		'7697' => '762',
		'7698' => '762',
		'7699' => '761',
		'7899' => '783',
		'7989' => '791',
		'7999' => '792',
		'8089' => '802',
		'8098' => '801',
		'8099' => '802',
		'8189' => '812',
		'8196' => '811',
		'8197' => '811',
		'8198' => '811',
		'8199' => '812',
		'8299' => '822',
		'8396' => '833',
		'8397' => '831',
		'8398' => '831',
		'8399' => '833',
		'8499' => '842',
		'8599' => '851',
		'8688' => '861',
		'8689' => '862',
		'8698' => '861',
		'8699' => '863',
		'8788' => '871',
		'8789' => '872',
		'8799' => '871',
		'8898' => '881',
		'8899' => '883',
		'8998' => '891',
		'8999' => '892',
		'9099' => '901',
		'9198' => '913',
		'9199' => '913',
		'9295' => '922',
		'9296' => '921',
		'9297' => '921',
		'9298' => '921',
		'9299' => '922',
		'9395' => '931',
		'9396' => '931',
		'9397' => '931',
		'9398' => '933',
		'9399' => '931',
		'9493' => '941',
		'9494' => '941',
		'9495' => '941',
		'9496' => '941',
		'9497' => '942',
		'9498' => '941',
		'9499' => '941',
		'9598' => '951',
		'9599' => '951',
		'97193' => '9712',
		'97194' => '9711',
		'97195' => '9711',
		'97196' => '9711',
		'97197' => '9711',
		'97198' => '9712',
		'97199' => '9711',
		'97491' => '9742',
		'97492' => '9744',
		'97493' => '9743',
		'97494' => '9741',
		'97495' => '9741',
		'97496' => '9741',
		'97497' => '9744',
		'97498' => '9742',
		'97499' => '9744',
		'97696' => '9761',
		'97697' => '9761',
		'97698' => '9761',
		'97699' => '9761',
	],
	2024 => [
		'1880' => '182', // Commune : Osmery ; Canton : Dun-sur-Auron ; Arrondissement = Saint-Amand-Montrond
		'6999' => '691', // Commune : Lyon ; Arrondissement : Lyon
		'7599' => '751', // Commune : Paris ; Arrondissement : Paris
	],
];
