<?php

// year min => year max => id comer => id cheflieu
// used in \PhpExtended\Insee\InseeMissingForeignKey
return [
	2022 => [
		2024 => [
			'975' => '97502',
			'977' => '97701',
			'978' => '97801',
			'984' => '98411',
			'986' => '98613',
			'987' => '98735',
			'988' => '98818',
			'989' => '98901',
		],
	],
];
