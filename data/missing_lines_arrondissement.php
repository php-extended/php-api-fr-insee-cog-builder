<?php

// year min => year max => idx => [data line]
// used in \PhpExtended\Insee\InseeMissingLines
// header ['id', 'fk_departement', 'fk_commune_cheflieu', 'fk_tncc', 'ncc', 'nccenr']
return [
	2008 => [
		2024 => [
			['9751', '975', '97501', '0', 'MIQUELON-LANGLADE', 'Miquelon-Langlade'],
			['9752', '975', '97502', '0', 'SAINT-PIERRE', 'Saint-Pierre'],
		],
	],
	2012 => [
		2024 => [
			['9761', '976', '97611', '0', 'MAMOUDZOU', 'Mamoudzou'],
		],
	],
	2022 => [
		2024 => [
			['9771', '977', '97701', '0', 'SAINT-BARTHELEMY', 'Saint-Barthélémy'],
			['9781', '978', '97801', '0', 'SAINT-MARTIN', 'Saint-Martin'],
			['9841', '984', '98411', '4', 'ILES SAINT-PAUL ET NOUVELLE-AMSTERDAM', 'Îles Saint-Paul et Nouvelle-Amsterdam'],
			['9842', '984', '98412', '5', 'ARCHIPEL DES KERGUELEN', 'Archipel des Kerguelen'],
			['9843', '984', '98413', '5', 'ARCHIPEL DES CROZET', 'Archipel des Crozet'],
			['9844', '984', '98414', '3', 'TERRE ADELIE', 'Terre-Adélie'],
			['9845', '984', '98415', '4', 'ILES EPARSES DE L OCEAN INDIEN', 'Îles Éparses de l\'océan Indien'],
			['9861', '986', '98611', '1', 'ALO', 'Alo'],
			['9862', '986', '98612', '0', 'SIGAVE', 'Sigave'],
			['9863', '986', '98613', '1', 'UVEA', 'Uvea'],
			['9871', '987', '98731', '4', 'ILES MARQUISES', 'Îles Marquises'],
			['9872', '987', '98711', '4', 'ILES TUAMOTU-GAMBIER', 'Îles Tuamotu-Gambier'],
			['9873', '987', '98735', '4', 'ILES DU VENT', 'Îles du Vent'],
			['9874', '987', '98758', '4', 'ILES SOUS-LE-VENT', 'Îles Sous-le-Vent'],
			['9875', '987', '98753', '4', 'ILES AUSTRALES', 'Îles Australes'],
			['9881', '988', '98818', '3', 'PROVINCE SUD', 'Province Sud'],
			['9882', '988', '98811', '3', 'PROVINCE NORD', 'Province Nord'],
			['9883', '988', '98814', '3', 'PROVINCE DES ILES LOYAUTE', 'Province des îles Loyauté'],
			['9891', '989', '98901', '0', 'CLIPPERTON', 'Clipperton'],
		],
	],
];
