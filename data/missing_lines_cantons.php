<?php

// year min => year max => idx => [data line]
// used in \PhpExtended\Insee\InseeMissingLines
// header ['id', 'fk_arrondissement', 'fk_composition_cantonale', 'fk_commune_cheflieu', 'fk_tncc', 'ncc', 'nccenr', 'fk_type_canton']
return [
	2008 => [
		2023 => [
			['97501', '9751', '1', '97501', '0', 'MIQUELON-LANGLADE', 'Miquelon-Langlade', 'N'],
			['97502', '9752', '1', '97502', '0', 'SAINT-PIERRE', 'Saint-Pierre', 'N'],
		],
	],
	2015 => [
		2023 => [
			['6915', '691', '1', '69123', '0', 'LYON', 'Lyon', 'N'],
			['7515', '751', '1', '75056', '0', 'PARIS', 'Paris', 'N'],
			['97201', '9721', '1', '97209', '0', 'FORT-DE-FRANCE', 'Fort-de-France', 'N'],
			['97301', '9731', '1', '97302', '0', 'CAYENNE', 'Cayenne', 'N'],
		],
	],
	2022 => [
		2023 => [
			['97711', '9771', '1', '97701', '0', 'SAINT-BARTHELEMY', 'Saint-Barthélémy', 'N'],
			['97811', '9781', '1', '97801', '0', 'SAINT-MARTIN', 'Saint-Martin', 'N'],
			['98411', '9841', '1', '98411', '4', 'ILES SAINT-PAUL ET NOUVELLE-AMSTERDAM', 'Îles Saint-Paul et Nouvelle-Amsterdam', 'N'],
			['98412', '9842', '1', '98412', '5', 'ARCHIPEL DES KERGUELEN', 'Archipel des Kerguelen', 'N'],
			['98413', '9843', '1', '98413', '5', 'ARCHIPEL DES CROZET', 'Archipel des Crozet', 'N'],
			['98414', '9844', '1', '98414', '3', 'TERRE ADELIE', 'Terre-Adélie', 'N'],
			['98415', '9845', '1', '98415', '4', 'ILES EPARSES DE L OCEAN INDIEN', 'Îles Éparses de l\'océan Indien', 'N'],
			['98611', '9861', '1', '98611', '1', 'ALO', 'Alo', 'N'],
			['98612', '9862', '1', '98612', '0', 'SIGAVE', 'Sigave', 'N'],
			['98613', '9863', '1', '98613', '1', 'UVEA', 'Uvea', 'N'],
			['98711', '9871', '1', '98731', '4', 'ILES MARQUISES', 'Îles Marquises', 'N'],
			['98712', '9872', '1', '98711', '4', 'ILES TUAMOTU-GAMBIER', 'Îles Tuamotu-Gambier', 'N'],
			['98713', '9873', '1', '98735', '4', 'ILES DU VENT', 'Îles du Vent', 'N'],
			['98714', '9874', '1', '98758', '4', 'ILES SOUS-LE-VENT', 'Îles Sous-le-Vent', 'N'],
			['98715', '9875', '1', '98753', '4', 'ILES AUSTRALES', 'Îles Australes', 'N'],
			['98811', '9881', '1', '98818', '3', 'PROVINCE SUD', 'Province Sud', 'N'],
			['98812', '9882', '1', '98811', '3', 'PROVINCE NORD', 'Province Nord', 'N'],
			['98813', '9883', '1', '98814', '3', 'PROVINCE DES ILES LOYAUTE', 'Province des îles Loyauté', 'N'],
			['98911', '9891', '1', '98901', '0', 'CLIPPERTON', 'Clipperton', 'N'],
		],
	],
];
