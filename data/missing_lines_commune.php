<?php

// year min => year max => id => [data line]
// used in \PhpExtended\Insee\InseeMissingLines
// header ['id', 'fk_type_commune', 'fk_departement', 'fk_commune_parent', 'fk_tncc', 'ncc', 'nccenr']
return [
	2008 => [
		2021 => [
			['97501', 'COM', '975', '', '0', 'MIQUELON-LANGLADE', 'Miquelon-Langlade'],
			['97502', 'COM', '975', '', '0', 'SAINT-PIERRE', 'Saint-Pierre'],
		],
	],
];
