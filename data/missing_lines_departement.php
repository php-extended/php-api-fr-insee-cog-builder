<?php

// year min => year max => id => [data line]
// used in \PhpExtended\Insee\InseeMissingLines
// header ['id', 'fk_region', 'fk_commune_cheflieu', 'fk_tncc', 'ncc', 'nccenr']
return [
	2008 => [
		2021 => [
			['975', '05', '97502', '0', 'SAINT-PIERRE-ET-MIQUELON', 'Saint-Pierre-et-Miquelon'],
		],
	],
];
