<?php

// year min => year max => idx => [data line]
// used in \PhpExtended\Insee\InseeMissingLines
// header ['id', 'fk_pays', 'fk_commune_cheflieu', 'fk_tncc', 'ncc', 'nccenr']
return [
	2008 => [
		2021 => [
			['05', 'XXXXX07', '97502', '0', 'SAINT-PIERRE-ET-MIQUELON', 'Saint-Pierre-et-Miquelon'],
		],
	],
];
