<?php

return [
	1999 => 'https://www.insee.fr/fr/statistiques/fichier/2560686/arrond1999-txt.zip',
	2000 => 'https://www.insee.fr/fr/statistiques/fichier/2560681/arrond2000-txt.zip',
	2001 => 'https://www.insee.fr/fr/statistiques/fichier/2560676/arrond2001-txt.zip',
	2002 => 'https://www.insee.fr/fr/statistiques/fichier/2560671/arrond2002-txt.zip',
	2003 => 'https://www.insee.fr/fr/statistiques/fichier/2560666/arrond2003-txt.zip',
	2004 => 'https://www.insee.fr/fr/statistiques/fichier/2560661/arrond2004-txt.zip',
	2005 => 'https://www.insee.fr/fr/statistiques/fichier/2560656/arrond2005-txt.zip',
	2006 => 'https://www.insee.fr/fr/statistiques/fichier/2560651/arrond2006-txt.zip',
	2007 => 'https://www.insee.fr/fr/statistiques/fichier/2560646/arrond2007-txt.zip',
	2008 => 'https://www.insee.fr/fr/statistiques/fichier/2560640/arrond2008-txt.zip',
	2009 => 'https://www.insee.fr/fr/statistiques/fichier/2560635/arrond2009-txt.zip',
	2010 => 'https://www.insee.fr/fr/statistiques/fichier/2560630/arrond2010-txt.zip',
	2011 => 'https://www.insee.fr/fr/statistiques/fichier/2560625/arrond2011-txt.zip',
	2012 => 'https://www.insee.fr/fr/statistiques/fichier/2560620/arrond2012-txt.zip',
	2013 => 'https://www.insee.fr/fr/statistiques/fichier/2560615/arrond2013-txt.zip',
	2014 => 'https://www.insee.fr/fr/statistiques/fichier/2560563/arrond2014-txt.zip',
	2015 => 'https://www.insee.fr/fr/statistiques/fichier/2560698/arrond2015-txt.zip',
	2016 => 'https://www.insee.fr/fr/statistiques/fichier/2114819/arrond2016-txt.zip',
	2017 => 'https://www.insee.fr/fr/statistiques/fichier/2666684/arrond2017-txt.zip',
	2018 => 'https://www.insee.fr/fr/statistiques/fichier/3363419/arrond2018-txt.zip',
	
	2019 => 'https://www.insee.fr/fr/statistiques/fichier/3720946/arrondissement2019-csv.zip',
	2020 => 'https://www.insee.fr/fr/statistiques/fichier/4316069/arrondissement2020-csv.zip',
	2021 => 'https://www.insee.fr/fr/statistiques/fichier/5057840/arrondissement2021-csv.zip',
	
	2022 => 'https://www.insee.fr/fr/statistiques/fichier/6051727/arrondissement_2022.csv',

	2023 => 'https://www.insee.fr/fr/statistiques/fichier/6800675/v_arrondissement_2023.csv',	
	2024 => 'https://www.insee.fr/fr/statistiques/fichier/7766585/v_arrondissement_2024.csv',
];
