<?php

return [
    2021 => 'https://www.insee.fr/fr/statistiques/fichier/5057840/CTCD2021-csv.zip',
    2022 => 'https://www.insee.fr/fr/statistiques/fichier/6051727/CTCD_2022.csv',

    2023 => 'https://www.insee.fr/fr/statistiques/fichier/6800675/v_CTCD_2023.csv',
    2024 => 'https://www.insee.fr/fr/statistiques/fichier/7766585/v_ctcd_2024.csv',
];
