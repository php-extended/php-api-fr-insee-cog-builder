<?php

return [
	1999 => 'https://www.insee.fr/fr/statistiques/fichier/2560686/comsimp1999-txt.zip',
	2000 => 'https://www.insee.fr/fr/statistiques/fichier/2560681/comsimp2000-txt.zip',
	2001 => 'https://www.insee.fr/fr/statistiques/fichier/2560676/comsimp2001-txt.zip',
	2002 => 'https://www.insee.fr/fr/statistiques/fichier/2560671/comsimp2002-txt.zip',
	2003 => 'https://www.insee.fr/fr/statistiques/fichier/2560666/comsimp2003-txt.zip',
	2004 => 'https://www.insee.fr/fr/statistiques/fichier/2560661/comsimp2004-txt.zip',
	2005 => 'https://www.insee.fr/fr/statistiques/fichier/2560656/comsimp2005-txt.zip',
	2006 => 'https://www.insee.fr/fr/statistiques/fichier/2560651/comsimp2006-txt.zip',
	2007 => 'https://www.insee.fr/fr/statistiques/fichier/2560646/comsimp2007-txt.zip',
	2008 => 'https://www.insee.fr/fr/statistiques/fichier/2560640/comsimp2008-txt.zip',
	2009 => 'https://www.insee.fr/fr/statistiques/fichier/2560635/comsimp2009-txt.zip',
	2010 => 'https://www.insee.fr/fr/statistiques/fichier/2560630/comsimp2010-txt.zip',
	2011 => 'https://www.insee.fr/fr/statistiques/fichier/2560625/comsimp2011-txt.zip',
	2012 => 'https://www.insee.fr/fr/statistiques/fichier/2560620/comsimp2012-txt.zip',
	2013 => 'https://www.insee.fr/fr/statistiques/fichier/2560615/comsimp2013-txt.zip',
	2014 => 'https://www.insee.fr/fr/statistiques/fichier/2560563/comsimp2014-txt.zip',
	2015 => 'https://www.insee.fr/fr/statistiques/fichier/2560698/comsimp2015-txt.zip',
	2016 => 'https://www.insee.fr/fr/statistiques/fichier/2114819/comsimp2016-txt.zip',
	2017 => 'https://www.insee.fr/fr/statistiques/fichier/2666684/comsimp2017-txt.zip',
	2018 => 'https://www.insee.fr/fr/statistiques/fichier/3363419/comsimp2018-txt.zip',
	
	2019 => 'https://www.insee.fr/fr/statistiques/fichier/3720946/communes-01042019-csv.zip',
	2020 => 'https://www.insee.fr/fr/statistiques/fichier/4316069/communes2020-csv.zip',
	2021 => 'https://www.insee.fr/fr/statistiques/fichier/5057840/commune2021-csv.zip',
	
	2022 => 'https://www.insee.fr/fr/statistiques/fichier/6051727/commune_2022.csv',

	2023 => 'https://www.insee.fr/fr/statistiques/fichier/6800675/v_commune_2023.csv',
	2024 => 'https://www.insee.fr/fr/statistiques/fichier/7766585/v_commune_2024.csv',
];
