<?php

return [
	2022 => 'https://www.insee.fr/fr/statistiques/fichier/6051727/com_comer_2022.csv',

	2023 => 'https://www.insee.fr/fr/statistiques/fichier/6800675/v_commune_comer_2023.csv',
	2024 => 'https://www.insee.fr/fr/statistiques/fichier/7766585/v_commune_comer_2024.csv',
];
