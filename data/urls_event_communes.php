<?php

return [
	1999 => 'https://www.insee.fr/fr/statistiques/fichier/2560686/historiq1999-txt.zip',
	2000 => 'https://www.insee.fr/fr/statistiques/fichier/2560681/historiq2000-txt.zip',
	2001 => 'https://www.insee.fr/fr/statistiques/fichier/2560676/historiq2001-txt.zip',
	2002 => 'https://www.insee.fr/fr/statistiques/fichier/2560671/historiq2002-txt.zip',
	2003 => 'https://www.insee.fr/fr/statistiques/fichier/2560666/historiq2003-txt.zip',
	2004 => 'https://www.insee.fr/fr/statistiques/fichier/2560661/historiq2004-txt.zip',
	2005 => 'https://www.insee.fr/fr/statistiques/fichier/2560656/historiq2005-txt.zip',
	2006 => 'https://www.insee.fr/fr/statistiques/fichier/2560651/historiq2006-txt.zip',
	2007 => 'https://www.insee.fr/fr/statistiques/fichier/2560646/historiq2007-txt.zip',
	2008 => 'https://www.insee.fr/fr/statistiques/fichier/2560640/historiq2008-txt.zip',
	2009 => 'https://www.insee.fr/fr/statistiques/fichier/2560635/historiq2009-txt.zip',
	2010 => 'https://www.insee.fr/fr/statistiques/fichier/2560630/historiq2010-txt.zip',
	2011 => 'https://www.insee.fr/fr/statistiques/fichier/2560625/historiq2011-txt.zip',
	2012 => 'https://www.insee.fr/fr/statistiques/fichier/2560620/historiq2012-txt.zip',
	2013 => 'https://www.insee.fr/fr/statistiques/fichier/2560615/historiq2013-txt.zip',
	2014 => 'https://www.insee.fr/fr/statistiques/fichier/2560563/historiq2014-txt.zip',
	2015 => 'https://www.insee.fr/fr/statistiques/fichier/2560698/historiq2015-txt.zip',
	
	2016 => 'https://www.insee.fr/fr/statistiques/fichier/2114819/historiq2016-txt.zip',
	2017 => 'https://www.insee.fr/fr/statistiques/fichier/2666684/historiq2017-txt.zip',
	2018 => 'https://www.insee.fr/fr/statistiques/fichier/3363419/historiq2018-txt.zip',
	
	2019 => 'https://www.insee.fr/fr/statistiques/fichier/3720946/mvtcommune-01042019-csv.zip',
	2020 => 'https://www.insee.fr/fr/statistiques/fichier/4316069/mvtcommune2020-csv.zip',
	2021 => 'https://www.insee.fr/fr/statistiques/fichier/5057840/mvtcommune2021-csv.zip',
	
	2022 => 'https://www.insee.fr/fr/statistiques/fichier/6051727/mvtcommune_2022.csv',

	2023 => 'https://www.insee.fr/fr/statistiques/fichier/6800675/v_mvtcommune_2023.csv',
	2024 => 'https://www.insee.fr/fr/statistiques/fichier/7766585/v_mvt_commune_2024.csv',
];
