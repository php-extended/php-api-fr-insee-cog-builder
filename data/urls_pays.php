<?php

return [
	1999 => 'https://www.insee.fr/fr/statistiques/fichier/2560686/pays1999-txt.zip',
	2000 => 'https://www.insee.fr/fr/statistiques/fichier/2560681/pays2000-txt.zip',
	2001 => 'https://www.insee.fr/fr/statistiques/fichier/2560676/pays2001-txt.zip',
	2002 => 'https://www.insee.fr/fr/statistiques/fichier/2560671/pays2002-txt.zip',
	2003 => 'https://www.insee.fr/fr/statistiques/fichier/2560666/pays2003-txt.zip',
	2004 => 'https://www.insee.fr/fr/statistiques/fichier/2560661/pays2004-txt.zip',
	2005 => 'https://www.insee.fr/fr/statistiques/fichier/2560656/pays2005-txt.zip',
	2006 => 'https://www.insee.fr/fr/statistiques/fichier/2560651/pays2006-txt.zip',
	2007 => 'https://www.insee.fr/fr/statistiques/fichier/2560646/pays2007-txt.zip',
	2008 => 'https://www.insee.fr/fr/statistiques/fichier/2560640/pays2008-txt.zip',
	2009 => 'https://www.insee.fr/fr/statistiques/fichier/2560635/pays2009-txt.zip',
	2010 => 'https://www.insee.fr/fr/statistiques/fichier/2560630/pays2010-txt.zip',
	2011 => 'https://www.insee.fr/fr/statistiques/fichier/2560625/pays2011-txt.zip',
	2012 => 'https://www.insee.fr/fr/statistiques/fichier/2560620/pays2012-txt.zip',
	2013 => 'https://www.insee.fr/fr/statistiques/fichier/2560615/pays2013-txt.zip',
	2014 => 'https://www.insee.fr/fr/statistiques/fichier/2560563/pays2014-txt.zip',
	
	2015 => 'https://www.insee.fr/fr/statistiques/fichier/2560698/pays2015-txt.zip',
	2016 => 'https://www.insee.fr/fr/statistiques/fichier/2114819/pays2016-txt.zip',
	2017 => 'https://www.insee.fr/fr/statistiques/fichier/2666684/pays2017-txt.zip',
	2018 => 'https://www.insee.fr/fr/statistiques/fichier/3363419/pays2018-txt.zip',
	
	2019 => 'https://www.insee.fr/fr/statistiques/fichier/3720946/pays2019-csv.zip',
	2020 => 'https://www.insee.fr/fr/statistiques/fichier/4316069/pays2020-csv.zip',
	2021 => 'https://www.insee.fr/fr/statistiques/fichier/5057840/pays2021-csv.zip',
	
	2022 => 'https://www.insee.fr/fr/statistiques/fichier/6051727/pays_2022.csv',
	2023 => 'https://www.insee.fr/fr/statistiques/fichier/6800675/v_pays_2023.csv',
	
	2024 => 'https://www.insee.fr/fr/statistiques/fichier/7766585/v_pays_territoire_2024.csv',
];
