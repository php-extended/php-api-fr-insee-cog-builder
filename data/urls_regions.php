<?php

return [
	1999 => 'https://www.insee.fr/fr/statistiques/fichier/2560686/reg1999-txt.zip',
	2000 => 'https://www.insee.fr/fr/statistiques/fichier/2560681/reg2000-txt.zip',
	2001 => 'https://www.insee.fr/fr/statistiques/fichier/2560676/reg2001-txt.zip',
	2002 => 'https://www.insee.fr/fr/statistiques/fichier/2560671/reg2002-txt.zip',
	2003 => 'https://www.insee.fr/fr/statistiques/fichier/2560666/reg2003-txt.zip',
	2004 => 'https://www.insee.fr/fr/statistiques/fichier/2560661/reg2004-txt.zip',
	2005 => 'https://www.insee.fr/fr/statistiques/fichier/2560656/reg2005-txt.zip',
	2006 => 'https://www.insee.fr/fr/statistiques/fichier/2560651/reg2006-txt.zip',
	2007 => 'https://www.insee.fr/fr/statistiques/fichier/2560646/reg2007-txt.zip',
	2008 => 'https://www.insee.fr/fr/statistiques/fichier/2560640/reg2008-txt.zip',
	2009 => 'https://www.insee.fr/fr/statistiques/fichier/2560635/reg2009-txt.zip',
	2010 => 'https://www.insee.fr/fr/statistiques/fichier/2560630/reg2010-txt.zip',
	2011 => 'https://www.insee.fr/fr/statistiques/fichier/2560625/reg2011-txt.zip',
	2012 => 'https://www.insee.fr/fr/statistiques/fichier/2560620/reg2012-txt.zip',
	2013 => 'https://www.insee.fr/fr/statistiques/fichier/2560615/reg2013-txt.zip',
	2014 => 'https://www.insee.fr/fr/statistiques/fichier/2560563/reg2014-txt.zip',
	2015 => 'https://www.insee.fr/fr/statistiques/fichier/2560698/reg2015-txt.zip',
	2016 => 'https://www.insee.fr/fr/statistiques/fichier/2114819/reg2016-txt.zip',
	2017 => 'https://www.insee.fr/fr/statistiques/fichier/2666684/reg2017-txt.zip',
	2018 => 'https://www.insee.fr/fr/statistiques/fichier/3363419/reg2018-txt.zip',
	
	2019 => 'https://www.insee.fr/fr/statistiques/fichier/3720946/region2019-csv.zip',
	2020 => 'https://www.insee.fr/fr/statistiques/fichier/4316069/region2020-csv.zip',
	2021 => 'https://www.insee.fr/fr/statistiques/fichier/5057840/region2021-csv.zip',
	
	2022 => 'https://www.insee.fr/fr/statistiques/fichier/6051727/region_2022.csv',
	
	2023 => 'https://www.insee.fr/fr/statistiques/fichier/6800675/v_region_2023.csv',
	2024 => 'https://www.insee.fr/fr/statistiques/fichier/7766585/v_region_2024.csv',
];
