<?php

return [
	"\x80" => '€',
	// \x81 unassigned
	"\x82" => '‚',
	"\x83" => 'ƒ',
	"\x84" => '„',
	"\x85" => '…',
	"\x86" => '†',
	"\x87" => '‡',
	"\x88" => 'ˆ',
	"\x89" => '‰',
	"\x8A" => 'Š',
	"\x8B" => '‹',
	"\x8C" => 'Œ',
	// \x8D unassigned
	"\x8E" => 'Ž',
	// \x8F unassigned
	
	// \x90 unassigned
	"\x91" => '‘',
	"\x92" => '’',
	"\x93" => '“',
	"\x94" => '”',
	"\x95" => '•',
	"\x96" => '–',
	"\x97" => '—',
	"\x98" => '˜',
	"\x99" => '™',
	"\x9A" => 'š',
	"\x9B" => '›',
	"\x9C" => 'œ',
	// \x9D unassigned
	"\x9E" => 'ž',
	"\x9F" => 'Ÿ',
	
	"\xA0" => ' ',
	"\xA1" => '¡',
	"\xA2" => '¢',
	"\xA3" => '£',
	"\xA4" => '€',
	"\xA5" => '¥',
	"\xA6" => 'Š',
	"\xA7" => '§',
	"\xA8" => 'š',
	"\xA9" => '©',
	"\xAA" => 'ª',
	"\xAB" => '«',
	"\xAC" => '¬',
	"\xAD" => '',
	"\xAE" => '®',
	"\xAF" => '¯',
	
	"\xB0" => '°',
	"\xB1" => '±',
	"\xB2" => '²',
	"\xB3" => '³',
	"\xB4" => 'Ž',
	"\xB5" => 'µ',
	"\xB6" => '¶',
	"\xB7" => '·',
	"\xB8" => 'ž',
	"\xB9" => '¹',
	"\xBA" => 'º',
	"\xBB" => '»',
	"\xBC" => 'Œ',
	"\xBD" => 'œ',
	"\xBE" => 'Ÿ',
	"\xBF" => '¿',
	
	"\xC0" => 'Î',
	"\xC1" => 'Á',
	"\xC2" => 'Â',
	"\xC3" => 'Ã',
	"\xC4" => 'Ä',
	"\xC5" => 'Å',
	"\xC6" => 'Æ',
	"\xC7" => 'Ç',
	"\xC8" => 'È',
	"\xC9" => 'É',
	"\xCA" => 'Ê',
	"\xCB" => 'Ë',
	"\xCC" => 'Ì',
	"\xCD" => 'Í',
	"\xCE" => 'Î',
	"\xCF" => 'Ï',
	
	"\xD0" => 'Ð',
	"\xD1" => 'Ñ',
	"\xD2" => 'Ò',
	"\xD3" => 'Ó',
	"\xD4" => 'Ô',
	"\xD5" => 'Õ',
	"\xD6" => 'Ö',
	"\xD7" => '×',
	"\xD8" => 'Ø',
	"\xD9" => 'Ù',
	"\xDA" => 'Ú',
	"\xDB" => 'Û',
	"\xDC" => 'Ü',
	"\xDD" => 'Ý',
	"\xDE" => 'Þ',
	"\xDF" => 'ß',
	
	"\xE0" => 'à',
	"\xE1" => 'á',
	"\xE2" => 'â',
	"\xE3" => 'ã',
	"\xE4" => 'ä',
	"\xE5" => 'å',
	"\xE6" => 'æ',
	"\xE7" => 'ç',
	"\xE8" => 'è',
	"\xE9" => 'é',
	"\xEA" => 'ê',
	"\xEB" => 'ë',
	"\xEC" => 'ì',
	"\xED" => 'í',
	"\xEE" => 'î',
	"\xEF" => 'ï',
	
	"\xF0" => 'ð',
	"\xF1" => 'ñ',
	"\xF2" => 'ò',
	"\xF3" => 'ó',
	"\xF4" => 'ô',
	"\xF5" => 'õ',
	"\xF6" => 'ö',
	"\xF7" => '÷',
	"\xF8" => 'ø',
	"\xF9" => 'ù',
	"\xFA" => 'ú',
	"\xFB" => 'û',
	"\xFC" => 'ü',
	"\xFD" => 'ý',
	"\xFE" => 'þ',
	"\xFF" => 'ÿ',
];
