<?php declare(strict_types=1);

/**
 * Download script for insee data.
 * 
 * This file will initiate replacement of files that are in the data folder.
 * 
 * Usage:
 * php download.php pay <yearMin>? <yearMax>?
 * php download.php region <yearMin>? <yearMax>?
 * php download.php departement <yearMin>? <yearMax>?
 * php download.php arrondissement <yearMin>? <yearMax>?
 * php download.php canton <yearMin>? <yearMax>?
 * php download.php commune <yearMin>? <yearMax>?
 * php download.php eventCommune
 * 
 * @author Anastaszor
 */

use PhpExtended\Endpoint\HttpEndpoint;
use PhpExtended\Endpoint\ZipHttpEndpoint;
use PhpExtended\HttpClient\ClientFactory;
use PhpExtended\Insee\InseeDownloader;
use PhpExtended\Logger\BasicConsoleLogger;

global $argv;

if(!isset($argv[1]))
{
	throw new InvalidArgumentException('The first argument should be the type of object to download');
}
$type = $argv[1];
$method = 'download'.ucfirst($type);

$composer = __DIR__.'/vendor/autoload.php';
if(!is_file($composer))
{
	throw new RuntimeException('You should run composer first.');
}
require $composer;

$logger = new BasicConsoleLogger();
$logger->setVerbosityLevel(2);
$clientFactory = new ClientFactory();
$clientFactory->setLogger($logger);
$clientFactory->getConfiguration()->disablePreferCurl();
$client = $clientFactory->createClient();

$endpoint = new InseeDownloader($client, null, null, $logger);

$rclass = new ReflectionClass($endpoint);
$rmethod = $rclass->getMethod($method);
if($rmethod === null)
{
	throw new InvalidArgumentException('The method "'.$method.'" does not exists into the endpoint.');
}
$parameters = $rmethod->getParameters();
$argcount = count($parameters);

$args = [];
for($i = 0; $i < $argcount; $i++)
{
	if(!isset($argv[2 + $i]))
	{
		if(!$parameters[$i]->allowsNull())
		{
			throw new InvalidArgumentException('The '.(2 + $i).'th argument should not be empty.');
		}
		
		$argv[2 + $i] = null;
	}
	$args[] = $argv[2 + $i] === null ? null : (int) $argv[2 + $i];
}

$result = call_user_func_array([$endpoint, $method], $args);

var_dump($result);

