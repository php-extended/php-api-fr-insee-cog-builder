<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-insee-cog-builder library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Insee;

use RuntimeException;
use Stringable;

/**
 * InseeAvailableId class file.
 * 
 * This represents the available ids from the current used data to double check
 * the consistency of the foreign keys of the files.
 * 
 * @author Anastaszor
 */
class InseeAvailableId implements Stringable
{
	
	/**
	 * Gets whether the list is properly loaded.
	 * 
	 * @var boolean
	 */
	protected bool $_loaded = false;
	
	/**
	 * The type for check for.
	 * 
	 * @var string
	 */
	protected string $_type;
	
	/**
	 * The year to check for.
	 * 
	 * @var integer
	 */
	protected int $_year;
	
	/**
	 * The list of available ids.
	 * 
	 * @var array<string, integer>
	 */
	protected array $_ids = [];
	
	/**
	 * Builds a new InseeAvailableId list with the given type and year.
	 * 
	 * @param string $type
	 * @param integer $year
	 */
	public function __construct(string $type, int $year)
	{
		$this->_type = $type;
		$this->_year = $year;
		$filePath = \dirname(__DIR__, 2).'/php-api-fr-insee-cog-object/data/'.((string) $year).'/'.$type.'.csv';
		if(!\is_file($filePath))
		{
			return;
		}
		
		$fileData = (string) \file_get_contents($filePath);
		
		foreach(\explode("\n", $fileData) as $fileDataLine)
		{
			$fileDataArr = \str_getcsv($fileDataLine, ',', '"', '\\');
			if(isset($fileDataArr[0]))
			{
				$this->_ids[(string) $fileDataArr[0]] = 1;
			}
		}
		
		$this->_loaded = true;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Gets whether this file is loaded.
	 * 
	 * @return boolean
	 */
	public function isLoaded() : bool
	{
		return $this->_loaded;
	}
	
	/**
	 * Gets whether the given foreign key is available.
	 * 
	 * @param string $foreignKey
	 * @return boolean
	 */
	public function isAvailable(string $foreignKey) : bool
	{
		return !$this->_loaded || isset($this->_ids[$foreignKey]);
	}
	
	/**
	 * Asserts that the given foreign key is available in the foreign key of ids
	 * or throws an exception if it is not.
	 * 
	 * @param string $foreignKey
	 * @return boolean true
	 * @throws RuntimeException
	 */
	public function assertAvailable(string $foreignKey) : bool
	{
		if($this->isAvailable($foreignKey))
		{
			return true;
		}
		
		$message = 'Failed to assert that foreign key {fk} is available in {type} for {year}';
		$context = [
			'{fk}' => $foreignKey,
			'{type}' => $this->_type,
			'{year}' => $this->_year,
		];
		
		throw new RuntimeException(\strtr($message, $context));
	}
	
}
