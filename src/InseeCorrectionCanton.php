<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-insee-cog-builder library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Insee;

use Stringable;

/**
 * InseeCorrectionCanton class file.
 * 
 * This class represents the corrections between cantons numbers.
 * 
 * @author Anastaszor
 * @todo deprecated
 */
class InseeCorrectionCanton implements Stringable
{
	
	/**
	 * The corrections. [minyear => [maxyear => [value => correction]]].
	 * 
	 * @var array<integer, array<integer, array<string, string>>>
	 */
	protected array $_corrections;
	
	/**
	 * Builds a new InseeCorrectionCanton with its inner data.
	 */
	public function __construct()
	{
		/** @psalm-suppress InvalidPropertyAssignmentValue */
		$this->_corrections = require \dirname(__DIR__).'/data/map_correction_canton.php';
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		$lines = [];
		
		foreach($this->_corrections as $minyear => $minyearCorrections)
		{
			foreach($minyearCorrections as $maxyear => $maxyearCorrections)
			{
				$lines[] = 'Between '.((string) $minyear).' and '.((string) $maxyear);
				
				foreach($maxyearCorrections as $old => $new)
				{
					$lines[] = "\t".$old.' => '.$new;
				}
			}
		}
		
		return \implode("\n", $lines)."\n";
	}
	
	/**
	 * Gets the value that is the corrected value of the given value for the
	 * given year, if it is defined, returns the original string if no
	 * corrections apply.
	 * 
	 * @param integer $year
	 * @param string $value
	 * @return string
	 */
	public function getCorrection(int $year, string $value) : string
	{
		foreach($this->_corrections as $minyear => $minyearCorrections)
		{
			if($minyear <= $year)
			{
				foreach($minyearCorrections as $maxyear => $maxyearCorrections)
				{
					if($maxyear >= $year)
					{
						if(isset($maxyearCorrections[$value]))
						{
							return $maxyearCorrections[$value];
						}
					}
				}
			}
		}
		
		return $value;
	}
	
}
