<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-insee-cog-builder library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Insee;

use Stringable;

/**
 * InseeCorrectionForeignKey class file.
 * 
 * This class is to correct the foreign key value from the primary
 * key of the object, for example :
 * - correct wrong arrondissement id from canton id
 * - correct wrong canton id from commune id
 * 
 * @author Anastaszor
 */
class InseeCorrectionForeignKey implements Stringable
{
	
	/**
	 * The map of corrections.
	 * year min => year max => primary key => foreign key.
	 * 
	 * @var array<integer, array<integer, array<string, string>>>
	 */
	protected array $_map;
	
	/**
	 * Builds a new InseeCorrectionForeignKey with its map name.
	 * 
	 * @param string $mapName
	 */
	public function __construct(string $mapName)
	{
		/** @psalm-suppress UnresolvableInclude */
		$this->_map = require \dirname(__DIR__).'/data/correction_'.$mapName.'.php';
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Gets the foreign key value from the year and primary key, or returns the
	 * default given foreign key if not found.
	 * 
	 * @param integer $year
	 * @param string $primaryKey
	 * @param string $foreignKey
	 * @return string
	 */
	public function getForeignKey(int $year, string $primaryKey, string $foreignKey) : string
	{
		foreach($this->_map as $yearMin => $innerMap)
		{
			if($yearMin <= $year)
			{
				foreach($innerMap as $yearMax => $valMap)
				{
					if($yearMax >= $year)
					{
						if(isset($valMap[$primaryKey]))
						{
							return $valMap[$primaryKey];
						}
					}
				}
			}
		}
		
		return $foreignKey;
	}
	
}
