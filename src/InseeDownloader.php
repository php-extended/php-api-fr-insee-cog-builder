<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-insee-cog-builder library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Insee;

use InvalidArgumentException;
use PhpExtended\HttpMessage\RequestFactory;
use PhpExtended\HttpMessage\UriFactory;
use PhpExtended\Parser\ParseThrowable;
use Psr\Http\Client\ClientExceptionInterface;
use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\RequestFactoryInterface;
use Psr\Http\Message\UriFactoryInterface;
use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;
use RuntimeException;
use Stringable;

/**
 * InseeDownloader class file.
 * 
 * This class downloads and transforms the data from the insee website to have
 * it ready to be exported as object iterators.
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.CouplingBetweenObjects")
 * @SuppressWarnings("PHPMD.ExcessiveClassComplexity")
 * @SuppressWarnings("PHPMD.StaticAccess")
 * @SuppressWarnings("PHPMD.ExcessiveClassLength")
 */
class InseeDownloader implements Stringable
{
	
	/**
	 * The http client.
	 *
	 * @var ClientInterface
	 */
	protected ClientInterface $_httpClient;
	
	/**
	 * The uri factory.
	 *
	 * @var UriFactoryInterface
	 */
	protected UriFactoryInterface $_uriFactory;
	
	/**
	 * The request factory.
	 *
	 * @var RequestFactoryInterface
	 */
	protected RequestFactoryInterface $_requestFactory;
	
	/**
	 * The logger.
	 * 
	 * @var LoggerInterface
	 */
	protected LoggerInterface $_logger;
	
	/**
	 * The code to utf8 replacement map.
	 * 
	 * @var array<string, string>
	 */
	protected array $_toUtf8ReplacementMap;
	
	/**
	 * Builds a new InseeDownloader with its inner data.
	 * 
	 * @param ClientInterface $httpClient
	 * @param ?UriFactoryInterface $uriFactory
	 * @param ?RequestFactoryInterface $requestFactory
	 * @param ?LoggerInterface $logger
	 * @throws RuntimeException
	 */
	public function __construct(
		ClientInterface $httpClient,
		?UriFactoryInterface $uriFactory = null,
		?RequestFactoryInterface $requestFactory = null,
		?LoggerInterface $logger = null
	) {
		$this->_httpClient = $httpClient;
		$this->_uriFactory = $uriFactory ?? new UriFactory();
		$this->_requestFactory = $requestFactory ?? new RequestFactory();
		$this->_logger = $logger ?? new NullLogger();
		$this->_toUtf8ReplacementMap = require \dirname(__DIR__).'/data/utf8_replacement_map.php';
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Downloads all the pays files and replaces those which are in the current
	 * repository folder.
	 * 
	 * @throws ClientExceptionInterface
	 * @throws InvalidArgumentException
	 * @throws ParseThrowable
	 * @throws RuntimeException
	 */
	public function downloadPays() : void
	{
		$dow = new InseeFileDownloader($this->_httpClient, $this->_uriFactory, $this->_requestFactory, 'pays', true);

		$path = \dirname(__DIR__, 2).'/php-api-fr-insee-cog-object/data/pays.csv';
		$this->ensureDirectory(\dirname($path));
		
		$headers = ['id', 'fk_actualite_pays', 'fk_pays_parent', 'creation_year', 'lib_cog', 'lib_enr', 'iso2', 'iso3', 'isonum3'];
		$futureFile = new InseeFileCsv(new InseeFileLine($headers), true);
		
		$lines = $dow->getRawLines($dow->getMaxYear());
		
		/** @var InseeFileLine $linedata */
		foreach($lines as $linedata)
		{	
			$futureFile->addLine($linedata);
		}
		
		$futureFile->writeToFile($path);
	}

	/**
	 * Downloads all the pays history file and replaces those which are in the
	 * current repository folder.
	 * 
	 * @throws ClientExceptionInterface
	 * @throws InvalidArgumentException
	 * @throws ParseThrowable
	 * @throws RuntimeException
	 */
	public function downloadPaysHistory() : void
	{
		$dow = new InseeFileDownloader($this->_httpClient, $this->_uriFactory, $this->_requestFactory, 'pays_history', false);
		
		$path = \dirname(__DIR__, 2).'/php-api-fr-insee-cog-object/data/pays_history.csv';
		$this->ensureDirectory(\dirname($path));

		$headers = ['fk_pays_id', 'fk_pays_before_id', 'lib_cog', 'lib_enr', 'date_start', 'date_end'];
		$futureFile = new InseeFileCsv(new InseeFileLine($headers), false);

		$lines = $dow->getRawLines($dow->getMaxYear());

		/** @var InseeFileLine $linedata */
		foreach($lines as $linedata)
		{
			$futureFile->addLine($linedata);
		}

		$futureFile->writeToFile($path);
	}
	
	/**
	 * Downloads all the regions files and replaces those which are in the
	 * current repository folder.
	 * 
	 * @param integer $minYear
	 * @param integer $maxYear
	 * @throws ClientExceptionInterface
	 * @throws InvalidArgumentException
	 * @throws ParseThrowable
	 * @throws RuntimeException
	 */
	public function downloadRegions(?int $minYear = null, ?int $maxYear = null) : void
	{
		$mapMisChfl = new InseeMissingForeignKey('cheflieu_outremer');
		$regdow = new InseeFileDownloader($this->_httpClient, $this->_uriFactory, $this->_requestFactory, 'regions');
		$oemdow = new InseeFileDownloader($this->_httpClient, $this->_uriFactory, $this->_requestFactory, 'outremer');
		$missingLines = new InseeMissingLines('region');
		
		$headers = ['id', 'fk_pays', 'fk_commune_cheflieu', 'fk_tncc', 'ncc', 'nccenr'];
		$headers = new InseeFileLine($headers);
		
		foreach($regdow->getYears($minYear, $maxYear) as $year)
		{
			$path = \dirname(__DIR__, 2).'/php-api-fr-insee-cog-object/data/'.((string) $year).'/region.csv';
			$this->ensureDirectory(\dirname($path));
			
			$availableFkPays = new InseeAvailableId('pays', $year);
			$availableFkCheflieu = new InseeAvailableId('commune', $year);
			
			$rawLines = $regdow->getRawLines($year);
			
			$futureFile = new InseeFileCsv($headers);
			
			/** @var InseeFileLine $linedata */
			foreach($rawLines as $linedata)
			{
				$paysId = '99100'; // france
				$availableFkPays->assertAvailable($paysId);
				$cheflieuId = \trim($linedata->get(1), '"');
				$availableFkCheflieu->assertAvailable($cheflieuId);
				
				$newparts = [
					\trim($linedata->get(0), '"'),
					$paysId,
					$cheflieuId,
					\trim($linedata->get(2), '"'),
					\trim($linedata->get(3), '"'),
					\trim($linedata->get(4), '"'),
				];
				
				$futureFile->addLineArray($newparts);
			}
			
			foreach($missingLines->getMissingLines($year) as $missingLine)
			{
				$futureFile->addLine($missingLine);
			}
			
			$oemlines = $oemdow->getRawLines($year);

			/** @var InseeFileLine $oemline */
			foreach($oemlines as $oemline)
			{
				$paysId = '99100'; // france
				$availableFkPays->assertAvailable($paysId);
				$cheflieuId = $mapMisChfl->getForeignKeyId($year, $oemline->get(0));
				$availableFkCheflieu->assertAvailable($cheflieuId);
				
				// [ID_COMER, TNCC, NCC, NCCENR, LIBELLE]
				$newline = [
					$this->getRegionId($oemline->get(0)),
					$paysId,
					$cheflieuId,
					$oemline->get(1),
					$oemline->get(2),
					$oemline->get(3),
				];
				
				$futureFile->addLineArray($newline);
			}
			
			$futureFile->writeToFile($path);
		}
	}
	
	/**
	 * Downloads all the departements files and replaces those which are in the
	 * current repository folder.
	 * 
	 * @param integer $minYear
	 * @param integer $maxYear
	 * @throws ClientExceptionInterface
	 * @throws InvalidArgumentException
	 * @throws ParseThrowable
	 * @throws RuntimeException
	 */
	public function downloadDepartements(?int $minYear = null, ?int $maxYear = null) : void
	{
		$depdow = new InseeFileDownloader($this->_httpClient, $this->_uriFactory, $this->_requestFactory, 'departements');
		$oemdow = new InseeFileDownloader($this->_httpClient, $this->_uriFactory, $this->_requestFactory, 'outremer');
		$mapMisChfl = new InseeMissingForeignKey('cheflieu_outremer');
		$missingLines = new InseeMissingLines('departement');
		
		$headers = ['id', 'fk_region', 'fk_commune_cheflieu', 'fk_tncc', 'ncc', 'nccenr'];
		$headers = new InseeFileLine($headers);
		
		foreach($depdow->getYears($minYear, $maxYear) as $year)
		{
			$path = \dirname(__DIR__, 2).'/php-api-fr-insee-cog-object/data/'.((string) $year).'/departement.csv';
			$this->ensureDirectory(\dirname($path));
			
// 			$availableFkRegion = new InseeAvailableId('region', $year);
// 			$availableFkCheflieu = new InseeAvailableId('commune', $year);
			
			$rawData = $depdow->getRawLines($year);
			
			$futureFile = new InseeFileCsv($headers);
			
			/** @var InseeFileLine $linedata */
			foreach($rawData as $linedata)
			{
				$newLine = $linedata->reindex(0, 1, 2, 3, 4, 5); // only keep 6 fields
				
				if(2019 > $year)
				{
					$newLine = $newLine->reindex(1, 0, 2, 3, 4, 5);
				}
				
				$futureFile->addLine($newLine);
			}
			
			foreach($missingLines->getMissingLines($year) as $missingLine)
			{
				$futureFile->addLine($missingLine);
			}
			
			$oemlines = $oemdow->getRawLines($year);

			/** @var InseeFileLine $oemline */
			foreach($oemlines as $oemline)
			{
				// [ID_COMER, TNCC, NCC, NCCENR, LIBELLE]
				$newLine = [
					$oemline->get(0),
					$this->getRegionId($oemline->get(0)),
					$mapMisChfl->getForeignKeyId($year, $oemline->get(0)),
					$oemline->get(1),
					$oemline->get(2),
					$oemline->get(3),
				];
				
				$futureFile->addLineArray($newLine);
			}
			
			$futureFile->writeToFile($path);
		}
	}
	
	/**
	 * Downloads all the arrondissements files and replaces those which are in 
	 * the current repository folder.
	 * 
	 * @param integer $minYear
	 * @param integer $maxYear
	 * @throws ClientExceptionInterface
	 * @throws InvalidArgumentException
	 * @throws ParseThrowable
	 * @throws RuntimeException
	 */
	public function downloadArrondissements(?int $minYear = null, ?int $maxYear = null) : void
	{
		$arrdow = new InseeFileDownloader($this->_httpClient, $this->_uriFactory, $this->_requestFactory, 'arrondissements');
		$missingLines = new InseeMissingLines('arrondissement');
		$missingCheflieus = new InseeMissingForeignKey('cheflieu_arrondissement');
		
		$headers = ['id', 'fk_departement', 'fk_commune_cheflieu', 'fk_tncc', 'ncc', 'nccenr'];
		$headers = new InseeFileLine($headers);
		
		foreach($arrdow->getYears($minYear, $maxYear) as $year)
		{
			$path = \dirname(__DIR__, 2).'/php-api-fr-insee-cog-object/data/'.((string) $year).'/arrondissement.csv';
			$this->ensureDirectory(\dirname($path));
			
// 			$availableFkDepartement = new InseeAvailableId('departement', $year);
// 			$availableFkCheflieu = new InseeAvailableId('commune', $year);
			
			$rawLines = $arrdow->getRawLines($year);
			
			$futureFile = new InseeFileCsv($headers);
			
			/** @var InseeFileLine $lineparts */
			foreach($rawLines as $lineparts)
			{
				if(2018 >= $year)
				{
					// && !empty to ignore the "06	976			0				" line in the 2012 file
					if($lineparts->countActiveFields() < 4)
					{
						continue;
					}
					
					$newline = [
						$lineparts->get(1).$lineparts->get(2),
						$lineparts->get(1),
						$lineparts->get(3),
						$lineparts->get(4),
						$lineparts->get(6),
						$lineparts->get(8),
					];
					
					if('' === $newline[2])
					{
						$newline[2] = $missingCheflieus->getForeignKeyId($year, $newline[0]);
					}
					
					$futureFile->addLineArray($newline);
					
					continue;
				}
				
				$newline = [
					$lineparts->get(0),
					$lineparts->get(1),
					$lineparts->get(3),
					$lineparts->get(4),
					$lineparts->get(5),
					$lineparts->get(6),
				];
				
				$futureFile->addLineArray($newline);
			}
			
			foreach($missingLines->getMissingLines($year) as $missingLine)
			{
				$futureFile->addLine($missingLine);
			}
			
			if(2024 < $year)
			{
				$this->_logger->warning('Verify that the arrondissement 9751/9761 are present in the files, otherwise correct the code to incude it');
				$this->_logger->warning(__CLASS__.' Line : '.((string) __LINE__));
			}
			
			$futureFile->writeToFile($path);
		}
	}
	
	/**
	 * Downloads all the cantons files and replaces those which are in the 
	 * current repository folder.
	 * 
	 * @param integer $minYear
	 * @param integer $maxYear
	 * @throws ClientExceptionInterface
	 * @throws InvalidArgumentException
	 * @throws ParseThrowable
	 * @throws RuntimeException
	 * @SuppressWarnings("PHPMD.CyclomaticComplexity")
	 * @SuppressWarnings("PHPMD.NPathComplexity")
	 * @SuppressWarnings("PHPMD.ExcessiveMethodLength")
	 */
	public function downloadCantons(?int $minYear = null, ?int $maxYear = null) : void
	{
		$missingLines = new InseeMissingLines('cantons');
		$corrIdArr = new InseeCorrectionForeignKey('canton_arrondissement');
		$corrIdChf = new InseeCorrectionForeignKey('canton_cheflieu');
		$missingChf = new InseeMissingForeignKey('cheflieu_canton');
		$candow = new InseeFileDownloader($this->_httpClient, $this->_uriFactory, $this->_requestFactory, 'cantons');
		
		$headers = ['id', 'fk_arrondissement', 'fk_composition_cantonale', 'fk_commune_cheflieu', 'fk_tncc', 'ncc', 'nccenr', 'fk_type_canton'];
		$headerLine = new InseeFileLine($headers);
		
		foreach($candow->getYears($minYear, $maxYear) as $year)
		{
			$path = \dirname(__DIR__, 2).'/php-api-fr-insee-cog-object/data/'.((string) $year).'/canton.csv';
			$this->ensureDirectory(\dirname($path));
			
// 			$availableFkArrondissement = new InseeAvailableId('arrondissement', $year);
// 			$availableFkCheflieu = new InseeAvailableId('commune', $year);
			
			$rawData = $candow->getRawLines($year);
			
			$futureFile = new InseeFileCsv($headerLine);
			
			$previousArrs = new InseePreviousArrondissement((int) $year);
			
			/** @var InseeFileLine $lineparts */
			foreach($rawData as $lineparts)
			{
				if(2014 >= $year)
				{
					$arrid = $lineparts->get(1).(empty($lineparts->get(2)) ? '1' : $lineparts->get(2));
					$newline = [
						$lineparts->get(1).$lineparts->get(3),
						$arrid,
						$lineparts->get(4),
						'751' === $arrid ? '75056' : $lineparts->get(5),
						$lineparts->get(6),
						$lineparts->get(8),
						$lineparts->get(10),
						'C',
					];
					
					$futureFile->addLineArray($newline);
					
					continue;
				}
				
				if(2018 >= $year)
				{
					$arrid = $lineparts->get(1).$lineparts->get(2);
					$newline = [
						$arrid,
						$corrIdArr->getForeignKey($year, $arrid, $previousArrs->getArrondissementNumber($arrid)),
						$lineparts->get(3),
						$corrIdChf->getForeignKey($year, $arrid, $lineparts->get(4)),
						$lineparts->get(5),
						$lineparts->get(7),
						$lineparts->get(9),
						'C',
					];
					
					$futureFile->addLineArray($newline);
					
					continue;
				}
				
				// {{{ TODO do for all lines
				// he who does not escape fields that contain a separator is a dummy
				$newparts = [];
				$key = 0;
				
				foreach($lineparts as $linepart)
				{
					if(\mb_substr($linepart, 0, 1) === ' ') // begins with a space
					{
						$newparts[$key - 1] = $newparts[$key - 1].','.$linepart;
						
						continue;
					}
					
					$newparts[$key] = $linepart;
					$key++;
				}
				
				$lineparts = new InseeFileLine($newparts);
				// }}} END TODO
				
				$newline = [
					$lineparts->get(0),
					$corrIdArr->getForeignKey($year, $lineparts->get(0), $previousArrs->getArrondissementNumber($lineparts->get(0))),
					(empty($lineparts->get(3)) ? '1' : $lineparts->get(3)),
					(empty($lineparts->get(4)) ? $missingChf->getForeignKeyId($year, $lineparts->get(0)) : $lineparts->get(4)),
					$lineparts->get(5),
					$lineparts->get(6),
					$lineparts->get(7),
					$lineparts->get(9),
				];
				
				$futureFile->addLineArray($newline);
			}
			
			foreach($missingLines->getMissingLines($year) as $missingLine)
			{
				$futureFile->addLine($missingLine);
			}
			
			if(2024 < $year)
			{
				$this->_logger->warning('Verify that the communes for dept 975 are present in the files, otherwise correct the code to incude it');
				$this->_logger->warning(__CLASS__.' Line : '.((string) __LINE__));
			}
			
			$futureFile->writeToFile($path);
		}
	}

	/**
	 * Downloads all the collectivites territoriales ayant competences departementales (ctcd) files and replaces those which are in the
	 * current repository folder.
	 * 
	 * @param integer $minYear
	 * @param integer $maxYear
	 * @throws ClientExceptionInterface
	 * @throws InvalidArgumentException
	 * @throws ParseThrowable
	 * @throws RuntimeException
	 * @SuppressWarnings("PHPMD.ExcessiveMethodLength")
	 * @SuppressWarnings("PHPMD.ElseExpression")
	 */
	public function downloadCollectivitesTerritoriales(?int $minYear = null, ?int $maxYear = null) : void
	{
		$missingChf = new InseeMissingForeignKey('cheflieu_collectivites');
		$ctcddow = new InseeFileDownloader($this->_httpClient, $this->_uriFactory, $this->_requestFactory, 'collectivites_territoriales');

		$headers = ['id', 'fk_region', 'fk_commune_cheflieu', 'fk_tncc', 'ncc', 'nccenr', 'libelle'];

		foreach($ctcddow->getYears($minYear, $maxYear) as $year)
		{
			$path = \dirname(__DIR__, 2).'/php-api-fr-insee-cog-object/data/'.((string) $year).'/collectivites_territoriales.csv';
			$this->ensureDirectory(\dirname($path));
			
			$rawData = $ctcddow->getRawLines($year);
			
			$futureFile = new InseeFileCsv(new InseeFileLine($headers));
			
			/** @var InseeFileLine $lineparts */
			foreach($rawData as $lineparts)
			{
				if(2024 > $year)
				{
					$newline = [
						$lineparts->get(0),
						$lineparts->get(1),
						$missingChf->getForeignKeyId($year, $lineparts->get(0)),
						$lineparts->get(2),
						$lineparts->get(3),
						$lineparts->get(4),
						$lineparts->get(5),
					];
				}
				else
				{
					$newline = [
						$lineparts->get(0),
						$lineparts->get(1),
						$lineparts->get(2),
						$lineparts->get(3),
						$lineparts->get(4),
						$lineparts->get(5),
						$lineparts->get(6),
					];
				}
				
				$futureFile->addLineArray($newline);
			}

			$futureFile->writeToFile($path);
		}
	}
	
	/**
	 * Downloads all the departements files and replaces those which are in the
	 * current repository folder.
	 * 
	 * @param integer $minYear
	 * @param integer $maxYear
	 * @throws ClientExceptionInterface
	 * @throws InvalidArgumentException
	 * @throws ParseThrowable
	 * @throws RuntimeException
	 * @SuppressWarnings("PHPMD.ExcessiveMethodLength")
	 */
	public function downloadCommunes(?int $minYear = null, ?int $maxYear = null) : void
	{
		$missingLines = new InseeMissingLines('commune');
// 		$missingCantons = new InseeMissingForeignKey('commune_canton');
		$comdow = new InseeFileDownloader($this->_httpClient, $this->_uriFactory, $this->_requestFactory, 'communes');
		$oemdow = new InseeFileDownloader($this->_httpClient, $this->_uriFactory, $this->_requestFactory, 'communes_outremer');
		
		$headers = ['id', 'fk_type_commune', 'fk_departement', 'fk_commune_parent', 'fk_tncc', 'ncc', 'nccenr'];
		$headers = new InseeFileLine($headers);
		
		foreach($comdow->getYears($minYear, $maxYear) as $year)
		{
			$path = \dirname(__DIR__, 2).'/php-api-fr-insee-cog-object/data/'.((string) $year).'/commune.csv';
			$this->ensureDirectory(\dirname($path));
			
			$lines = $comdow->getRawLines($year);
			
			$futureFile = new InseeFileCsv($headers, false);
			
			// refill missing canton fk
			$prevCan = null;
			
			/** @var InseeFileLine $lineparts */
			foreach($lines as $lineparts)
			{
				switch(true)
				{
					case 2019 > $year:
						
						$newline = [
							$lineparts->get(3).$lineparts->get(4),
							'COM',
							$lineparts->get(3),
							'',
							$lineparts->get(7),
							$lineparts->get(9),
							$lineparts->get(11),
						];
						break;
						
					case 2021 > $year:
						$newline = [
							$lineparts->get(1),
							$lineparts->get(0),
							$lineparts->get(3),
							$lineparts->get(10),
							$lineparts->get(5),
							$lineparts->get(6),
							$lineparts->get(7),
						];
						break;
						
					default:
						$newline = [
							$lineparts->get(1),
							$lineparts->get(0),
							$lineparts->get(3),
							$lineparts->get(11),
							$lineparts->get(6),
							$lineparts->get(7),
							$lineparts->get(8),
						];
						break;
				}
				
				if('' === $newline[2])
				{
					$newline[2] = $prevCan;
				}
				$prevCan = $newline[2];
				
				$futureFile->addLineArray($newline);
			}
			
			// management communes outre mer
			$oemlines = $oemdow->getRawLines($year);
			
			/** @var InseeFileLine $oemline */
			foreach($oemlines as $oemline)
			{
				$newline = [
					$oemline->get(0),
					$oemline->get(5),
					$oemline->get(6),
					'',
					$oemline->get(1),
					$oemline->get(2),
					$oemline->get(3),
				];
				
				$futureFile->addLineArray($newline);
			}
			
			foreach($missingLines->getMissingLines($year) as $line)
			{
				$futureFile->addLine($line);
			}
			
			if(2021 < $year)
			{
				$this->_logger->warning('Verify that the communes for dept 975 are present in the files, otherwise correct the code to incude it');
				$this->_logger->warning(__CLASS__.' Line : '.((string) __LINE__));
			}
			
			$futureFile->writeToFile($path);
		}
	}
	
	/**
	 * Downloads all the events on communes files and replaces those which are
	 * in the current repository folder.
	 * 
	 * @throws ClientExceptionInterface
	 * @throws InvalidArgumentException
	 * @throws ParseThrowable
	 * @throws RuntimeException
	 */
	public function downloadEventCommunes() : void
	{
		$evtdow = new InseeFileDownloader($this->_httpClient, $this->_uriFactory, $this->_requestFactory, 'event_communes');
		
		$path = \dirname(__DIR__, 2).'/php-api-fr-insee-cog-object/data/event_commune.csv';
		$this->ensureDirectory(\dirname($path));
		
		$headers = ['date_effet', 'fk_type_event_commune', 'fk_type_commune_before', 'fk_commune_before', 'fk_type_commune_after', 'fk_commune_after', 'fk_tncc_before', 'ncc_before', 'nccenr_before', 'fk_tncc_after', 'ncc_after', 'nccenr_after'];
		$futureFile = new InseeFileCsv(new InseeFileLine($headers), false);
		
		$lines = $evtdow->getRawLines($evtdow->getMaxYear());
		
		/** @var InseeFileLine $linedata */
		foreach($lines as $linedata)
		{
			if($linedata->get(4) === '' || $linedata->get(5) === '') // if before is empty we dont care
			{
				continue;
			}

			$newline = $linedata->reindex(1, 0, 2, 3, 8, 9, 4, 5, 6, 10, 11, 12);
			
			$futureFile->addLine($newline);
		}
		
		$futureFile->writeToFile($path);
	}

	/**
	 * Builds the 2 digit id of the region of the outremer department.
	 * 97X => 0X
	 * 98X => 1X.
	 * 
	 * @param string $outremerId
	 * @return string
	 */
	public function getRegionId(?string $outremerId) : string
	{
		return \str_pad((string) (((int) $outremerId) - 970), 2, '0', \STR_PAD_LEFT);
	}
	
	/**
	 * Ensures that the given directory is created and writeable.
	 * 
	 * @param string $dirPath
	 * @throws RuntimeException
	 */
	protected function ensureMakeDirectory(string $dirPath) : void
	{
		if('/' === $dirPath || '.' === $dirPath || empty($dirPath))
		{
			return;
		}
		
		$parentPath = \dirname($dirPath);
		$this->ensureMakeDirectory($parentPath);
		
		if(!\is_dir($dirPath))
		{
			if(!\mkdir($dirPath))
			{
				$message = 'Failed to create {path} folder';
				$context = ['{path}' => $dirPath];
				
				throw new RuntimeException(\strtr($message, $context));
			}
		}
	}
	
	/**
	 * Ensures that a directory exists at given path.
	 * 
	 * @param string $dirPath
	 * @throws RuntimeException if the directory does not exists and cannot be
	 *                          created
	 */
	protected function ensureDirectory(string $dirPath) : void
	{
		if(!\is_dir($dirPath))
		{
			if(!\mkdir($dirPath))
			{
				throw new RuntimeException(\strtr('Failed to make directory at {path}', ['{path}' => $dirPath]));
			}
		}
	}
	
}
