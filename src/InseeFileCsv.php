<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-insee-cog-builder library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Insee;

use RuntimeException;
use Stringable;

/**
 * InseeFileCsv class file.
 * 
 * This class represents a file with all its lines.
 * 
 * @author Anastaszor
 */
class InseeFileCsv implements Stringable
{
	
	/**
	 * The header line of the file.
	 * 
	 * @var InseeFileLine
	 */
	protected InseeFileLine $_headers;
	
	/**
	 * @var boolean
	 */
	protected bool $_enforceUnique = true;
	
	/**
	 * The lines of the file.
	 * 
	 * @var array<integer|string, InseeFileLine>
	 */
	protected array $_lines = [];
	
	/**
	 * Whether the lines are sorted.
	 * 
	 * @var boolean
	 */
	protected bool $_sorted = false;
	
	/**
	 * Builds a new InseeFileCsv with the given header line.
	 * 
	 * @param InseeFileLine $headers
	 * @param boolean $enforceUnique
	 */
	public function __construct(InseeFileLine $headers, bool $enforceUnique = true)
	{
		$this->_headers = $headers;
		$this->_enforceUnique = $enforceUnique;
		$this->_sorted = false;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		if(!$this->_sorted)
		{
			if($this->_enforceUnique)
			{
				\ksort($this->_lines, \SORT_STRING);
			}
			
			if(!$this->_enforceUnique)
			{
				\usort($this->_lines, function(InseeFileLine $line1, InseeFileLine $line2) : int
				{
					$max = (int) \max($line1->count(), $line2->count());
					$cmp = 0;
					
					for($idx = 0; $idx <= $max; $idx++)
					{
						$cmp = \strcmp($line1->get($idx), $line2->get($idx));
						if(0 !== $cmp)
						{
							return $cmp;
						}
					}
					
					return $cmp;
				});
			}
			
			$this->_sorted = true;
		}
		
		$data = [$this->_headers->toLower()->toCsvLine()];
		
		foreach($this->_lines as $line)
		{
			$data[] = $line->toCsvLine();
		}
		
		return \implode("\n", $data)."\n";
	}
	
	/**
	 * Adds all the lines of data to the csv file.
	 * 
	 * @param array<integer|string, array<integer|string, null|string>> $datas
	 * @return InseeFileCsv
	 * @throws RuntimeException in case of duplicated ids
	 */
	public function addAllLineArray(array $datas) : InseeFileCsv
	{
		foreach($datas as $data)
		{
			$this->addLineArray($data);
		}
		
		return $this;
	}
	
	/**
	 * Adds a line to the csv file as array string data.
	 * 
	 * @param array<integer|string, null|string> $data
	 * @return InseeFileCsv
	 * @throws RuntimeException in case of duplicated ids
	 */
	public function addLineArray(array $data) : InseeFileCsv
	{
		return $this->addLine(new InseeFileLine($data));
	}
	
	/**
	 * Adds the given lines to the csv file.
	 * 
	 * @param array<integer|string, InseeFileLine> $lines
	 * @return InseeFileCsv
	 * @throws RuntimeException in case of duplicated ids
	 */
	public function addAllLines(array $lines) : InseeFileCsv
	{
		foreach($lines as $line)
		{
			$this->addLine($line);
		}
		
		return $this;
	}
	
	/**
	 * Adds a line to the csv file.
	 * 
	 * @param InseeFileLine $line
	 * @return InseeFileCsv
	 * @throws RuntimeException in case of duplicated ids
	 */
	public function addLine(InseeFileLine $line) : InseeFileCsv
	{
		if(!$line->isEmpty())
		{
			if($this->_enforceUnique)
			{
				$primaryKey = $line->get(0);
				if(isset($this->_lines[$primaryKey]))
				{
					$message = 'Duplicate primary key "{pk}"';
					$context = ['{pk}' => $primaryKey];
					
					throw new RuntimeException(\strtr($message, $context));
				}
				
				$this->_lines[$primaryKey] = $line;
			}
			
			if(!$this->_enforceUnique)
			{
				$this->_lines[] = $line;
			}
			
			$this->_sorted = false;
		}
		
		return $this;
	}
	
	/**
	 * Gets whether this file has the line with the given primary key.
	 * 
	 * @param string $primaryKey
	 * @return boolean
	 */
	public function hasLine(string $primaryKey) : bool
	{
		return isset($this->_lines[$primaryKey]);
	}
	
	/**
	 * Writes the contents of the csv lines to the file at given path.
	 * 
	 * @param string $path
	 * @return integer the number of bytes written
	 * @throws RuntimeException
	 */
	public function writeToFile(string $path) : int
	{
		$bytes = \file_put_contents($path, $this->__toString());
		
		if(false === $bytes)
		{
			$message = 'Failed to write file at {path}';
			$context = ['{path}' => $path];
			
			throw new RuntimeException(\strtr($message, $context));
		}
		
		return $bytes;
	}
	
}
