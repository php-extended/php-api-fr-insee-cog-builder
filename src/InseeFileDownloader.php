<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-insee-cog-builder library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Insee;

use InvalidArgumentException;
use PhpExtended\HttpMessage\RequestFactory;
use PhpExtended\HttpMessage\UriFactory;
use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\RequestFactoryInterface;
use Psr\Http\Message\UriFactoryInterface;
use RuntimeException;
use Stringable;

/**
 * InseeFileDownloader class file.
 * 
 * This class is a transformer that transforms the abstract distant resource
 * into the raw data it contains, in order to normalize it over the years.
 * 
 * @author Anastaszor
 */
class InseeFileDownloader implements Stringable
{
	
	/**
	 * The http client.
	 *
	 * @var ClientInterface
	 */
	protected ClientInterface $_httpClient;
	
	/**
	 * The uri factory.
	 *
	 * @var UriFactoryInterface
	 */
	protected UriFactoryInterface $_uriFactory;
	
	/**
	 * The request factory.
	 *
	 * @var RequestFactoryInterface
	 */
	protected RequestFactoryInterface $_requestFactory;
	
	/**
	 * The code to utf8 replacement map.
	 *
	 * @var array<string, string>
	 */
	protected array $_toUtf8Map;
	
	/**
	 * The name of the url map.
	 * 
	 * @var string
	 */
	protected string $_urlMapName;
	
	/**
	 * The map of the urls to download the csv files.
	 *
	 * @var array<integer, string>
	 */
	protected array $_urlMap = [];
	
	/**
	 * The map pays.
	 * 
	 * @var InseeMapPays
	 */
	protected InseeMapPays $_mapPays;
	
	/**
	 * Gets whether this file downloader needs sorting.
	 * 
	 * @var boolean
	 */
	protected bool $_needSort = false;
	
	/**
	 * Builds a new InseeFileDownloader with its args.
	 * 
	 * @param ClientInterface $httpClient
	 * @param ?UriFactoryInterface $uriFactory
	 * @param ?RequestFactoryInterface $requestFactory
	 * @param string $urlMapName
	 * @param boolean $needSort
	 */
	public function __construct(
		ClientInterface $httpClient,
		?UriFactoryInterface $uriFactory,
		?RequestFactoryInterface $requestFactory,
		string $urlMapName,
		bool $needSort = false
	) {
		$this->_httpClient = $httpClient;
		$this->_uriFactory = $uriFactory ?? new UriFactory();
		$this->_requestFactory = $requestFactory ?? new RequestFactory();
		$this->_toUtf8Map = require \dirname(__DIR__).'/data/utf8_replacement_map.php';
		/** @psalm-suppress UnresolvableInclude */
		$this->_urlMap = require \dirname(__DIR__).'/data/urls_'.$urlMapName.'.php';
		$this->_urlMapName = $urlMapName;
		$this->_mapPays = new InseeMapPays();
		$this->_needSort = $needSort;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Gets the available min year for the pays.
	 *
	 * @return integer
	 */
	public function getMinYear() : int
	{
		$years = \array_keys($this->_urlMap);
		
		return \count($years) > 0 ? \min($years) : 0;
	}
	
	/**
	 * Gets the available max year for the pays.
	 *
	 * @return integer
	 */
	public function getMaxYear() : int
	{
		$years = \array_keys($this->_urlMap);
		
		return \count($years) > 0 ? \max($years) : 0;
	}
	
	/**
	 * Gets all the years that are between the min and max year, limited
	 * by the available years.
	 * 
	 * @param ?integer $minYear
	 * @param ?integer $maxYear
	 * @return array<integer, integer>
	 */
	public function getYears(?int $minYear, ?int $maxYear) : array
	{
		$years = [];
		
		$minYear = null === $minYear ? $this->getMinYear() : $minYear;
		$maxYear = null === $maxYear ? $this->getMaxYear() : $maxYear;
		
		foreach(\array_keys($this->_urlMap) as $year)
		{
			if($year >= $minYear && $year <= $maxYear)
			{
				$years[] = $year;
			}
		}
		
		return $years;
	}
	
	/**
	 * Gets the data lines from the raw downloaded csv string.
	 * 
	 * @param integer $year the year to consider
	 * @param string $data the raw data
	 * @return array<integer, InseeFileLine>
	 */
	public function getUtf8Lines(int $year, string $data) : array
	{
		if(2019 > $year)
		{
			$data = $this->transformToUtf8($data);
		}
		
		$lines = \explode("\n", $data);
		
		/** @var array<integer, InseeFileLine> $newlines */
		$newlines = [];
		
		foreach($lines as $linedata)
		{
			if(2019 > $year)
			{
				$newline = \explode("\t", $linedata);
				if(\count($newline) > 1)
				{
					$newlines[] = new InseeFileLine($newline);
				}
				
				continue;
			}
			
			$newline = [];
			
			foreach(\str_getcsv($linedata, ',', '"', '\\') as $value)
			{
				$newline[] = (string) $value;
			}
			
			if(\count($newline) > 1)
			{
				$newlines[] = new InseeFileLine($newline);
			}
		}
		
		return $newlines;
	}
	
	/**
	 * Gets the parsed lines data from the raw files. The format of the lines
	 * depends of the year that is used to fetch the data.
	 * On inexistant years, an empty iterator is returned.
	 *
	 * @param integer $year
	 * @return InseeRawIterator
	 * @throws \PhpExtended\Parser\ParseThrowable
	 * @throws \Psr\Http\Client\ClientExceptionInterface
	 * @throws InvalidArgumentException
	 * @throws RuntimeException
	 */
	public function getRawLines(int $year) : InseeRawIterator
	{
		if(!isset($this->_urlMap[$year]))
		{
			return new InseeRawIterator(new InseeFileLine([]), []);
		}
		
		\sleep(1);
		
		$url = $this->_urlMap[$year];
		$uri = $this->_uriFactory->createUri($url);
		$request = $this->_requestFactory->createRequest('GET', $uri);
		if(\preg_match('#.zip$#', $url))
		{
			$request = $request->withAddedHeader('X-Php-Download-File', $this->getTempPath().'/'.$this->_urlMapName.'_'.((string) $year).'.zip');
		}
		$response = $this->_httpClient->sendRequest($request);
		$data = $response->getBody()->__toString();
		
		$newlines = $this->getUtf8Lines($year, $data);
		
		// removes the header line
		$header = \array_shift($newlines) ?? new InseeFileLine([]);
		
		if($this->_needSort)
		{
			$newlines = $this->sortLines($newlines);
		}
		
		return new InseeRawIterator($header, $newlines);
	}
	
	/**
	 * Sorts the given lines.
	 * 
	 * @param array<integer, InseeFileLine> $lines
	 * @return array<integer, InseeFileLine>
	 * @throws InvalidArgumentException
	 */
	public function sortLines(array $lines) : array
	{
		\usort($lines, [$this, 'compareLines']);
		
		return $lines;
	}
	
	/**
	 * Compares two lines.
	 *
	 * @param InseeFileLine $line1
	 * @param InseeFileLine $line2
	 * @return integer -1 if $line1 < $line2, +1 if $line1 > $line2, 0 otherwise
	 * @throws InvalidArgumentException if the country ids are not set or do not exist
	 */
	public function compareLines(InseeFileLine $line1, InseeFileLine $line2) : int
	{
		// [0] is the comparator for COG values
		$compCog = \strcmp($line1->get(0), $line2->get(0));
		if(0 !== $compCog)
		{
			return $compCog;
		}
		
		$compActual = \strcmp($line1->get(1), $line2->get(1));
		if(0 !== $compActual)
		{
			return $compActual;
		}
		
		if('XXXXX' === $line1->get(0))
		{
			$score1 = $this->_mapPays->getPaysId($line1->get(5));
			$score2 = $this->_mapPays->getPaysId($line2->get(5));
			
			return ((int) $score1) - ((int) $score2);
		}
		
		return \strcmp($line1->get(5), $line2->get(5));
	}
	
	/**
	 * Transforms to utf8 the data from whatever charset it is at first
	 * (some mix between ISO-8859-15 and CP1252).
	 *
	 * @param string $data
	 * @return string
	 */
	protected function transformToUtf8(string $data) : string
	{
		return \strtr($data, $this->_toUtf8Map);
	}
	
	/**
	 * Gets a suitable directory to download and uncompress data.
	 *
	 * @return string
	 * @throws RuntimeException
	 */
	protected function getTempPath() : string
	{
		$base = \is_dir('/media/anastaszor/RUNTIME/') ? '/media/anastaszor/RUNTIME/' : '/tmp/';
		$real = $base.'php-extended__php-api-fr-insee-cog-builder';
		if(!\is_dir($real))
		{
			if(!\mkdir($real))
			{
				throw new RuntimeException('Failed to make temp directory at '.$real);
			}
		}
		
		return $real;
	}
	
}
