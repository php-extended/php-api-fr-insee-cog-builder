<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-insee-cog-builder library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Insee;

use ArrayIterator;
use Countable;
use Iterator;
use IteratorAggregate;
use Stringable;

/**
 * InseeFileLine class file.
 * 
 * This class represents a file line.
 * 
 * @author Anastaszor
 * @implements \IteratorAggregate<integer, string>
 */
class InseeFileLine implements Countable, IteratorAggregate, Stringable
{
	
	/**
	 * The data of the line.
	 * 
	 * @var array<integer, string>
	 */
	protected array $_data = [];
	
	/**
	 * Builds a new InseeFileLine with its inner data.
	 * 
	 * @param array<integer|string, null|string> $data
	 */
	public function __construct(array $data)
	{
		foreach($data as $value)
		{
			$this->_data[] = \rtrim((string) $value);
		}
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return $this->toCsvLine();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Countable::count()
	 */
	public function count() : int
	{
		return \count($this->_data);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \IteratorAggregate::getIterator()
	 * @return Iterator<integer, string>
	 */
	public function getIterator() : Iterator
	{
		return new ArrayIterator($this->_data);
	}
	
	/**
	 * Gets whether this line is empty.
	 * 
	 * @return boolean
	 */
	public function isEmpty() : bool
	{
		return $this->countActiveFields() === 0;
	}
	
	/**
	 * Counts the number of active fields in this record.
	 * 
	 * @return integer
	 */
	public function countActiveFields() : int
	{
		$count = 0;
		
		foreach($this->_data as $datum)
		{
			if(!empty($datum))
			{
				$count++;
			}
		}
		
		return $count;
	}
	
	/**
	 * Gets a new line with all the values passed through strtolower().
	 *
	 * @return InseeFileLine
	 */
	public function toLower() : InseeFileLine
	{
		$data = [];
		
		foreach($this->_data as $value)
		{
			$data[] = (string) \mb_strtolower($value);
		}
		
		return new self($data);
	}
	
	/**
	 * Gets a new line with all the values passed through strtoupper().
	 * 
	 * @return InseeFileLine
	 */
	public function toUpper() : InseeFileLine
	{
		$data = [];
		
		foreach($this->_data as $value)
		{
			$data[] = (string) \mb_strtoupper($value);
		}
		
		return new self($data);
	}
	
	/**
	 * Gets the array form of this line.
	 *
	 * @return array<integer, string>
	 */
	public function toArray() : array
	{
		return $this->_data;
	}
	
	/**
	 * Gets a csv representation of this line.
	 * 
	 * @return string
	 */
	public function toCsvLine() : string
	{
		return \str_replace(',""', ',', '"'.\implode('","', $this->_data).'"');
	}
	
	/**
	 * Gets the data at the given index, empty string if not set.
	 * 
	 * @param integer $index
	 * @return string
	 */ 
	public function get(int $index) : string
	{
		return $this->_data[$index] ?? '';
	}
	
	/**
	 * Builds a new InseeFileLine based with the array of reindexes.
	 * 
	 * @param integer ...$indexes
	 * @return InseeFileLine
	 */
	public function reindex(int ...$indexes) : InseeFileLine
	{
		$data = [];
		
		foreach($indexes as $index)
		{
			$data[] = $this->get($index);
		}
		
		return new self($data);
	}
	
}
