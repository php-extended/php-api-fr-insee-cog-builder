<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-insee-cog-builder library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Insee;

use InvalidArgumentException;
use Stringable;

/**
 * InseeMapPays class file.
 * 
 * This class represents all the ids of the pays from their labels, for
 * normalization purposes.
 * 
 * @author Anastaszor
 * @todo normalize in missing fk or correction fk
 */
class InseeMapPays implements Stringable
{
	
	/**
	 * The map of the pays.
	 * 
	 * @var array<string, integer>
	 */
	protected array $_map = [];
	
	/**
	 * Builds a new InseeMapPays.
	 */
	public function __construct()
	{
		$this->_map = require \dirname(__DIR__).'/data/map_pays_ids.php';
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Gets whether the libelle is known.
	 * 
	 * @param string $libelle
	 * @return boolean
	 */
	public function existsPaysId(string $libelle) : bool
	{
		return isset($this->_map[$libelle]);
	}
	
	/**
	 * Gets the id of the pays from the given libelle.
	 * 
	 * @param ?string $libelle
	 * @return string
	 * @throws InvalidArgumentException
	 */
	public function getPaysId(?string $libelle) : string
	{
		$libelle = (string) $libelle;
		if('' !== $libelle && isset($this->_map[$libelle]))
		{
			return (string) $this->_map[$libelle];
		}
		
		$message = 'Failed to find map value "{value}" in file {path}';
		$context = [
			'{value}' => '' === $libelle ? '(empty)' : $libelle,
			'{path}' => \dirname(__DIR__).'/data/map_pays_ids.php',
		];
		
		throw new InvalidArgumentException(\strtr($message, $context));
	}
	
}
