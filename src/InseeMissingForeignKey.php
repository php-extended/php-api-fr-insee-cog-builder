<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-insee-cog-builder library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Insee;

use InvalidArgumentException;
use Stringable;

/**
 * InseeMissingForeignKey class file.
 * 
 * This class is a map for missing cheflieus
 * 
 * @author Anastaszor
 */
class InseeMissingForeignKey implements Stringable
{
	
	/**
	 * The map of missing cheflieus, by year.
	 * 
	 * @var array<integer, array<integer, array<string, string>>>
	 */
	protected array $_map;
	
	/**
	 * The name of the map.
	 * 
	 * @var string
	 */
	protected string $_mapName;
	
	/**
	 * Builds a new InseeMissingOutremerCheflieu with the map.
	 * 
	 * @param string $mapName the name of the map for missing cheflieus
	 */
	public function __construct(string $mapName)
	{
		$this->_mapName = $mapName;
		/** @psalm-suppress UnresolvableInclude */
		$this->_map = require \dirname(__DIR__).'/data/missing_fks_'.$mapName.'.php';
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Gets the outremer cheflieu or throws if not found.
	 * 
	 * @param integer $year
	 * @param string $primaryKey
	 * @return string
	 * @throws InvalidArgumentException
	 */
	public function getForeignKeyId(int $year, string $primaryKey) : string
	{
		foreach($this->_map as $minYear => $innermap)
		{
			if($minYear <= $year)
			{
				foreach($innermap as $maxYear => $idmap)
				{
					if($maxYear >= $year)
					{
						if(isset($idmap[$primaryKey]))
						{
							return $idmap[$primaryKey];
						}
					}
				}
			}
		}
		
		$message = 'Failed to find missing foreign key for {mapname} {sid} and year {year}';
		$context = ['{year}' => $year, '{mapname}' => $this->_mapName, '{sid}' => $primaryKey];
		
		throw new InvalidArgumentException(\strtr($message, $context));
	}
	
}
