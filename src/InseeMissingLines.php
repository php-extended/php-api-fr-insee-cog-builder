<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-insee-cog-builder library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Insee;

use Stringable;

/**
 * InseeMissingLines class file.
 * 
 * This class represents all the lines that are missing from the official files
 * to be added on the normalized data files.
 * 
 * @author Anastaszor
 */
class InseeMissingLines implements Stringable
{
	
	/**
	 * The missing lines.
	 * year min => year max => idx => [data line].
	 * 
	 * @var array<integer, array<integer, array<integer, array<integer, string>>>>
	 */
	protected array $_missingLines;
	
	/**
	 * Gets the missing lines from the given file.
	 * 
	 * @param string $missingLinesFileName
	 */
	public function __construct(string $missingLinesFileName)
	{
		/** @psalm-suppress UnresolvableInclude */
		$this->_missingLines = require \dirname(__DIR__).'/data/missing_lines_'.$missingLinesFileName.'.php';
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Gets the missing lines for the given year.
	 * 
	 * @param integer $year
	 * @return array<integer, InseeFileLine>
	 */
	public function getMissingLines(int $year) : array
	{
		$lines = [];
		
		foreach($this->_missingLines as $minYear => $innerMap)
		{
			if($minYear <= $year)
			{
				foreach($innerMap as $maxYear => $dataLines)
				{
					if($maxYear >= $year)
					{
						foreach($dataLines as $dataLine)
						{
							$lines[] = new InseeFileLine($dataLine);
						}
					}
				}
			}
		}
		
		return $lines;
	}
	
}
