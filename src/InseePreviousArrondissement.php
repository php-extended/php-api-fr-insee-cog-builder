<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-insee-cog-builder library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Insee;

use RuntimeException;
use Stringable;

/**
 * InseePreviousArrondissement class file.
 * 
 * This class registers all the Arrondissement numbers from the previous years.
 * 
 * @author Anastaszor
 * @todo normalize in missing fk or correction fk
 */
class InseePreviousArrondissement implements Stringable
{
	
	/**
	 * The map of previous Arrondissement numbers to previous arrondissement numbers.
	 * 
	 * @var array<string, string>
	 */
	protected array $_map = [];
	
	/**
	 * The map of missing Arrondissement ids.
	 * 
	 * @var array<string, string>
	 */
	protected array $_missing = [];
	
	/**
	 * Builds a new InseePreviousArrondissement with the given current year.
	 * 
	 * @param integer $year
	 */
	public function __construct(int $year)
	{
		$prevArrPath = \dirname(__DIR__, 2).'/php-api-fr-insee-cog-object/data/'.((string) ($year - 1)).'/canton.csv';
		if(\is_file($prevArrPath))
		{
			$prevArrDataStr = (string) \file_get_contents($prevArrPath);
			
			foreach(\explode("\n", $prevArrDataStr) as $prevArrDataLine)
			{
				$prevDataArr = \str_getcsv($prevArrDataLine, ',', '"', '\\');
				if(isset($prevDataArr[0], $prevDataArr[1]))
				{
					$this->_map[(string) $prevDataArr[0]] = (string) $prevDataArr[1];
				}
			}
		}
		
		/** @var array<integer, array<string, string>> $missingArrIds */
		$missingArrIds = require \dirname(__DIR__).'/data/map_missing_arrondissement_ids.php';
		if(isset($missingArrIds[$year]))
		{
			$this->_missing = $missingArrIds[$year];
		}
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Gets the arrondissement number from the previous Arrondissement number or the missing
	 * cant.
	 * 
	 * @param string $arrNumber
	 * @return string
	 * @throws RuntimeException
	 */
	public function getArrondissementNumber(string $arrNumber) : string
	{
		if(isset($this->_map[$arrNumber]))
		{
			return $this->_map[$arrNumber];
		}
		
		if(isset($this->_missing[$arrNumber]))
		{
			return $this->_missing[$arrNumber];
		}
		
		$message = 'Failed to find arrondissement id for {arrid}';
		$context = ['{arrid}' => $arrNumber];
		
		throw new RuntimeException(\strtr($message, $context));
	}
	
}
