<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-insee-cog-builder library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Insee;

use ArrayIterator;
use Stringable;

/**
 * InseeRawIterator class file.
 * 
 * This represents an iterator over the dataset of values that are included
 * into a downloaded file.
 * 
 * @author Anastaszor
 * @extends \ArrayIterator<integer, InseeFileLine>
 * @todo rename insee file line iterator
 */
class InseeRawIterator extends ArrayIterator implements Stringable
{
	
	/**
	 * The headers of this iterator.
	 * 
	 * @var InseeFileLine
	 */
	protected InseeFileLine $_header;
	
	/**
	 * Builds a new InseeRawIterator with the header and pre-sorted data values.
	 * 
	 * @param InseeFileLine $header
	 * @param array<integer, InseeFileLine> $data
	 */
	public function __construct(InseeFileLine $header, array $data)
	{
		$this->_header = $header;
		parent::__construct($data);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Gets whether this iterator is empty.
	 * 
	 * @return boolean
	 */
	public function isEmpty() : bool
	{
		return $this->_header->isEmpty();
	}
	
	/**
	 * Gets the header for this iterator.
	 * 
	 * @return InseeFileLine
	 */
	public function getHeader() : InseeFileLine
	{
		return $this->_header;
	}
	
}
