<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-insee-cog-builder library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Insee\InseeAvailableId;
use PHPUnit\Framework\TestCase;

/**
 * InseeAvailableIdTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Insee\InseeAvailableId
 *
 * @internal
 *
 * @small
 */
class InseeAvailableIdTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var InseeAvailableId
	 */
	protected InseeAvailableId $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testInexistantFile() : void
	{
		$object = new InseeAvailableId('pays', 1900);
		$this->assertTrue($object->isAvailable('toto'));
	}
	
	public function testIsAvailable() : void
	{
		$this->assertTrue($this->_object->isAvailable('XXXXX01'));
	}
	
	public function testAssertAvailable() : void
	{
		$this->assertTrue($this->_object->assertAvailable('XXXXX02'));
	}
	
	public function testAssertNotAvailable() : void
	{
		if($this->_object->isLoaded())
		{
			$this->expectException(RuntimeException::class);
		}
		
		$this->assertTrue($this->_object->assertAvailable('XXXXXXXX'));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new InseeAvailableId('pays', 2020);
	}
	
}
