<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-insee-cog-builder library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Insee\InseeCorrectionCanton;
use PHPUnit\Framework\TestCase;

/**
 * InseeCorrectionCantonTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Insee\InseeCorrectionCanton
 *
 * @internal
 *
 * @small
 */
class InseeCorrectionCantonTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var InseeCorrectionCanton
	 */
	protected InseeCorrectionCanton $_object;
	
	public function testToString() : void
	{
		$this->assertStringContainsString('Between', $this->_object->__toString());
	}
	
	public function testGetCorrection01() : void
	{
		$this->assertEquals('0123', $this->_object->getCorrection(1999, '0198'));
	}
	
	public function testGetCorrection02() : void
	{
		$this->assertEquals('1000', $this->_object->getCorrection(1995, '1000'));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new InseeCorrectionCanton();
	}
	
}
