<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-insee-cog-builder library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Insee\InseeCorrectionForeignKey;
use PHPUnit\Framework\TestCase;

/**
 * InseeCorrectionForeignKeyTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Insee\InseeCorrectionForeignKey
 *
 * @internal
 *
 * @small
 */
class InseeCorrectionForeignKeyTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var InseeCorrectionForeignKey
	 */
	protected InseeCorrectionForeignKey $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testGetForeignKeySuccess() : void
	{
		$this->assertEquals('572', $this->_object->getForeignKey(2015, '5703', '571'));
	}
	
	public function testGetForeignKeyFailed() : void
	{
		$this->assertEquals('571', $this->_object->getForeignKey(2014, '5703', '571'));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new InseeCorrectionForeignKey('canton_arrondissement');
	}
	
}
