<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-insee-cog-builder library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Insee\InseeFileCsv;
use PhpExtended\Insee\InseeFileLine;
use PHPUnit\Framework\TestCase;

/**
 * InseeFileCsvTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Insee\InseeFileCsv
 *
 * @internal
 *
 * @small
 */
class InseeFileCsvTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var InseeFileCsv
	 */
	protected InseeFileCsv $_object;
	
	public function testToString() : void
	{
		$expected = '"header"'."\n"
			.'"First","Second","Third"'."\n"
			.'"Fourth","Fifth"'."\n"
			.'"Second","Third","Fourth"'."\n";
		$this->assertEquals($expected, $this->_object->__toString());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new InseeFileCsv(new InseeFileLine(['Header']));
		$this->_object->addLineArray(['First', 'Second', 'Third']);
		$this->_object->addLineArray(['Second', 'Third', 'Fourth']);
		$this->_object->addLineArray(['Fourth', 'Fifth']);
	}
	
}
