<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-insee-cog-builder library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Insee\InseeFileDownloader;
use PhpExtended\Insee\InseeFileLine;
use PhpExtended\Insee\InseeRawIterator;
use PHPUnit\Framework\TestCase;
use Psr\Http\Client\ClientInterface;

/**
 * InseeFileDownloaderTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Insee\InseeFileDownloader
 *
 * @internal
 *
 * @small
 */
class InseeFileDownloaderTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var InseeFileDownloader
	 */
	protected InseeFileDownloader $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testGetMinYear() : void
	{
		$this->assertEquals(1999, $this->_object->getMinYear());
	}
	
	public function testGetMaxYear() : void
	{
		$this->assertGreaterThan(2000, $this->_object->getMaxYear());
	}
	
	public function testGetYears() : void
	{
		$this->assertGreaterThan(1, \count($this->_object->getYears(null, null)));
	}
	
	public function testGetInexistantRawLines() : void
	{
		$this->assertEquals(new InseeRawIterator(new InseeFileLine([]), []), $this->_object->getRawLines(1900));
	}
	
	public function testCompareLinesCog() : void
	{
		$line1 = new InseeFileLine(['990001']);
		$line2 = new InseeFileLine(['990002']);
		$this->assertLessThanOrEqual(-1, $this->_object->compareLines($line1, $line2));
	}
	
	public function testCompareLinesAct() : void
	{
		$line1 = new InseeFileLine(['990001', '990010']);
		$line2 = new InseeFileLine(['990001', '990020']);
		$this->assertLessThanOrEqual(-1, $this->_object->compareLines($line1, $line2));
	}
	
	public function testCompareLinesLib() : void
	{
		$line1 = new InseeFileLine(['XXXXX', '', '', '', '', 'GUADELOUPE']);
		$line2 = new InseeFileLine(['XXXXX', '', '', '', '', 'MARTINIQUE']);
		$this->assertLessThanOrEqual(-1, $this->_object->compareLines($line1, $line2));
	}
	
	public function testCompareLinesDefault() : void
	{
		$line1 = new InseeFileLine(['990001', '990010', '', '', '', 'DANEMARK']);
		$line2 = new InseeFileLine(['990001', '990010', '', '', '', 'GROENLAND']);
		$this->assertLessThanOrEqual(-1, $this->_object->compareLines($line1, $line2));
	}
	
	public function testSortLines() : void
	{
		$lines = [
			new InseeFileLine(['990001', '990010', '', '', '', 'GROENLAND']),
			new InseeFileLine(['990001', '990010', '', '', '', 'DANEMARK']),
		];
		
		$expected = [
			new InseeFileLine(['990001', '990010', '', '', '', 'DANEMARK']),
			new InseeFileLine(['990001', '990010', '', '', '', 'GROENLAND']),
		];
		
		$this->assertEquals($expected, $this->_object->sortLines($lines));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new InseeFileDownloader(
			$this->getMockForAbstractClass(ClientInterface::class),
			null,
			null,
			'pays',
		);
	}
	
}
