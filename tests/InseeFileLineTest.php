<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-insee-cog-builder library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Insee\InseeFileLine;
use PHPUnit\Framework\TestCase;

/**
 * InseeFileLineTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Insee\InseeFileLine
 *
 * @internal
 *
 * @small
 */
class InseeFileLineTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var InseeFileLine
	 */
	protected InseeFileLine $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('"First","Second","Third"', $this->_object->__toString());
	}
	
	public function testCount() : void
	{
		$this->assertEquals(3, $this->_object->count());
	}
	
	public function testGetIterator() : void
	{
		$this->assertEquals(new ArrayIterator(['First', 'Second', 'Third']), $this->_object->getIterator());
	}
	
	public function testIsEmpty() : void
	{
		$this->assertFalse($this->_object->isEmpty());
	}
	
	public function testToLower() : void
	{
		$this->assertEquals(new InseeFileLine(['first', 'second', 'third']), $this->_object->toLower());
	}
	
	public function testToUpper() : void
	{
		$this->assertEquals(new InseeFileLine(['FIRST', 'SECOND', 'THIRD']), $this->_object->toUpper());
	}
	
	public function testToArray() : void
	{
		$this->assertEquals(['First', 'Second', 'Third'], $this->_object->toArray());
	}
	
	public function testToCsvLine() : void
	{
		$this->assertEquals('"First","Second","Third"', $this->_object->toCsvLine());
	}
	
	public function testGetDataAtIndex() : void
	{
		$this->assertEquals('Second', $this->_object->get(1));
	}
	
	public function testReindex() : void
	{
		$this->assertEquals(new InseeFileLine(['Third', 'First', 'Second']), $this->_object->reindex(2, 0, 1));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new InseeFileLine(['First', 'Second', 'Third']);
	}
	
}
