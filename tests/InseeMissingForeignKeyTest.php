<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-insee-cog-builder library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Insee\InseeMissingForeignKey;
use PHPUnit\Framework\TestCase;

/**
 * InseeMissingForeignKeyTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Insee\InseeMissingForeignKey
 *
 * @internal
 *
 * @small
 */
class InseeMissingForeignKeyTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var InseeMissingForeignKey
	 */
	protected InseeMissingForeignKey $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testGetForeignKeyId() : void
	{
		$this->assertEquals('97502', $this->_object->getForeignKeyId(2022, '975'));
	}
	
	public function testFailedGetForeignKeyId() : void
	{
		$this->expectException(InvalidArgumentException::class);
		
		$this->_object->getForeignKeyId(1900, '10000');
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new InseeMissingForeignKey('cheflieu_outremer');
	}
	
}
