<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-insee-cog-builder library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Insee\InseePreviousArrondissement;
use PHPUnit\Framework\TestCase;

/**
 * InseePreviousArrondissementTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Insee\InseePreviousArrondissement
 *
 * @internal
 *
 * @small
 */
class InseePreviousArrondissementTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var InseePreviousArrondissement
	 */
	protected InseePreviousArrondissement $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new InseePreviousArrondissement(((int) \date('Y')) - 1);
	}
	
}
