<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-insee-cog-builder library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Insee\InseeFileLine;
use PhpExtended\Insee\InseeRawIterator;
use PHPUnit\Framework\TestCase;

/**
 * InseeRawIteratorTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Insee\InseeRawIterator
 *
 * @internal
 *
 * @small
 */
class InseeRawIteratorTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var InseeRawIterator
	 */
	protected InseeRawIterator $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testIsEmpty() : void
	{
		$this->assertTrue($this->_object->isEmpty());
	}
	
	public function testGetHeader() : void
	{
		$this->assertEquals(new InseeFileLine([]), $this->_object->getHeader());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new InseeRawIterator(new InseeFileLine([]), []);
	}
	
}
